import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './commons/home/home/home.component';
import {NavbarComponent} from './commons/navbar/navbar/navbar.component';
import {environment} from "../environments/environment.prod";
import {StoreModule} from "@ngrx/store";
import {appReducer} from "./app.reducer";
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {EffectsModule} from "@ngrx/effects";
import {AuthEffects} from "./commons/auth/store/auth.effects";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {OwnersEffects} from "./modules/owner/store/owners.effects";
import {TokenInterceptor} from "./commons/token.interceptor";
import {DoctorAuthGuardService} from "./commons/auth-guard/doctor.auth-guard.service";
import {PetEffects} from "./modules/pet/store/pet.effects";
import {AlertComponent} from './commons/alert/alert.component';
import {VisitEffects} from "./modules/visit/store/visit.effects";
import {DoctorEffects} from "./modules/doctor/store/doctor.effects";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AlertComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(appReducer),
    StoreDevtoolsModule.instrument({
      logOnly: environment.production
    }),
    EffectsModule.forRoot([AuthEffects, OwnersEffects, PetEffects, VisitEffects, DoctorEffects])
  ],
  providers: [
    DoctorAuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
