import {ActionReducerMap} from "@ngrx/store";
import * as fromAuth from "./commons/auth/store/auth.reducers"
import * as fromAlert from "./commons/alert/store/alert.reducers"
import * as fromOwner from "./modules/owner/store/owner.reducers"
import * as fromDoctor from "./modules/doctor/store/doctor.reducers"
import * as fromPet from "./modules/pet/store/pet.reducers"
import * as fromVisit from "./modules/visit/store/visit.reducers"

export interface AppState {
  auth: fromAuth.AuthState
  owner: fromOwner.OwnerState
  pet: fromPet.PetState
  alert: fromAlert.AlertState
  visit: fromVisit.VisitState
  doctor: fromDoctor.DoctorState
}

export const appReducer: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer,
  owner: fromOwner.ownerReducer,
  pet: fromPet.petReducer,
  alert: fromAlert.alertReducer,
  visit: fromVisit.visitReducer,
  doctor: fromDoctor.doctorReducer
};
