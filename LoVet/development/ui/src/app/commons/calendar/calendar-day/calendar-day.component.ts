import {Component, Input, OnInit} from '@angular/core';
import * as visitActions from "../../../modules/visit/store/visit.actions"
import {Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {VisitService} from "../../../modules/visit/service/visit.service";
import {VisitState} from "../../../modules/visit/store/visit.reducers";
import {Store} from "@ngrx/store";
import {Doctor} from "../../../modules/doctor/model/doctor";
import {Observable} from "rxjs";


@Component({
  selector: 'app-calendar-day',
  templateUrl: './calendar-day.component.html',
  styleUrls: ['./calendar-day.component.css'],
  providers: [DatePipe]
})
export class CalendarDayComponent implements OnInit {
  allVisitHours: string[];
  doctors: Doctor[];

  @Input()
  visitDate: Date;

  constructor(private store: Store<VisitState>, private visitService: VisitService, private router: Router
    , private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.allVisitHours = ['05:00', '05:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00',
      '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00'];
  }

  isBooked() {
    //TODO
    return false;
  }

  isInThePast(currentHour: string) {
    this.visitDate.setHours(Number(String(currentHour).split(':')[0]),
      Number(String(currentHour).split(':')[1]), 0, 0);
    return new Date() > this.visitDate;
  }

  onBookVisitClicked(hour: string) {
    let hours = hour.split(':')[0];
    let minutes = hour.split(':')[1];
    this.visitDate.setHours(Number(hours) + 2, Number(minutes));

    this.store.dispatch(new visitActions.StoreBookingDate(this.transformDateToString(this.visitDate)));
    this.router.navigate(['/visit/book/confirm'])
  }

  private transformDateToString(visitDate: Date): string {
    return this.datePipe.transform(visitDate, 'yyyy-MM-dd HH:mm', 'GMT+0')
  }


  createStringDateTime(visitHour: string) {
    let visitDateTimeString = this.visitDate.getUTCFullYear() + '-';

    if (this.visitDate.getMonth() < 10) {
      visitDateTimeString += '0' + (this.visitDate.getMonth() + 1) + '-'
    } else {
      visitDateTimeString += (this.visitDate.getMonth() + 1) + '-'
    }

    if (this.visitDate.getDate() < 10) {
      visitDateTimeString += '0' + this.visitDate.getDate()
    } else {
      visitDateTimeString += this.visitDate.getDate()
    }

    visitDateTimeString += ' ' + visitHour;

    console.log(visitDateTimeString);
    return visitDateTimeString
  }
}
