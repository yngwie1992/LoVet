import { AlertActions, AlertActionTypes } from './alert.actions';
import {Alert} from "../model/alert";

export interface AlertState {
  alerts: Alert[]
}

const initialState: AlertState = {
  alerts: []
};

export function alertReducer(state = initialState, action: AlertActions) {
  switch(action.type) {
    case AlertActionTypes.CLEAR_ALERTS: {
      return {
        ...state,
        alerts: []
      };
    }
    case AlertActionTypes.ADD_ALERTS: {
      return {
        ...state,
        alerts: [...state.alerts, action.alert]
      };
    }
    default:
      return {...state};
  }
}
