import {createFeatureSelector, createSelector} from "@ngrx/store";
import {AlertState} from "./alert.reducers";


export const getAlertFeatureState = createFeatureSelector<AlertState>(
  "alert"
);

export const getAlerts = createSelector(
  getAlertFeatureState,
  (state: AlertState) => state.alerts
);
