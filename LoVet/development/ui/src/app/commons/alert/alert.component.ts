import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../app.reducer";
import {Observable} from "rxjs";
import * as alertSelectors from "./store/alert.selectors"
import {Alert} from "./model/alert";


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  private alerts$: Observable<Alert[]>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.alerts$ = this.store.select(alertSelectors.getAlerts)
  }
}
