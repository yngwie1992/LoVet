import {Injectable} from "@angular/core";
import {AuthService} from "./auth/service/auth.service";
import {HttpInterceptor} from "@angular/common/http/src/interceptor";
import {HttpEvent, HttpHandler, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.authService.getToken() !== null) {
      request = request.clone({
        setHeaders: {
          Authorization: "Bearer " + this.authService.getToken()
        }
      });
    }
    return next.handle(request);
  }
  // when token expires force to log in
  //https://medium.com/@ryanchenkie_40935/angular-authentication-using-the-http-client-and-http-interceptors-2f9d1540eb8

}

