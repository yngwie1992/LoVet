import {Injectable} from '@angular/core';
import {User} from "../model/user";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public http: HttpClient) {
  }

  login(user: User) {
    return this.http.post('/api/auth', user )
  }

  register(user: User) {
    return this.http.post('/api/owners/register', user)
  }

  getToken(): string {
    return localStorage.getItem('token');
  }
}
