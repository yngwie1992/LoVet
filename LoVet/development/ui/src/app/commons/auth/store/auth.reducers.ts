import {AuthActions, AuthActionTypes} from "./auth.actions";

export interface AuthState {
  isLoggedIn: boolean
  isLoginFailed: boolean
  username: string
  role: string
}

export const initState: AuthState = {
  isLoggedIn: false,
  isLoginFailed: false,
  username: '',
  role: ''

};

export function authReducer(state = initState, action: AuthActions) {
  switch (action.type) {
    case AuthActionTypes.LOG_OUT: {
      localStorage.clear();
      return {
        ...state,
        isLoggedIn: false,
        username: '',
        role: ''
      }
    }

    case AuthActionTypes.LOG_IN_SUCCESS: {
      return {
        ...state,
        isLoggedIn: true,
        username: action.username,
        role: action.role
      }
    }
    case AuthActionTypes.LOG_IN_FAIL: {
      return {
        ...state,
        isLoggedIn: false,
        isLoginFailed: true
      }
    }
    default:
      return state;
  }
}
