import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {Action} from "@ngrx/store";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {Router} from "@angular/router";
import * as authActions from "./auth.actions";
import {AuthActionTypes, TryLogIn, TryRegister} from "./auth.actions";
import {catchError, map, mergeMap} from "rxjs/operators";
import {AuthService} from "../service/auth.service";
import {HttpErrorResponse} from "@angular/common/http";
import {AlertService} from "../../alert/alert.service";

@Injectable()
export class AuthEffects {

  constructor(private actions$: Actions, private router: Router,
              private authService: AuthService, private alertService: AlertService) {
  }

  @Effect()
  login: Observable<Action> = this.actions$.pipe(ofType(AuthActionTypes.TRY_LOG_IN),
    mergeMap((actionInput: TryLogIn) =>
      this.authService.login(actionInput.user).pipe(
        map(response => {
          localStorage.setItem('token', response['token']);
          this.router.navigate(['/']);
          return new authActions.LoginSuccess(response['username'], response['role'])
        })
      )
    ), catchError((response: HttpErrorResponse) => {
      this.alertService.errorAlert(response.error);
      setTimeout(function() {
        window.location.reload();
      }, 1500);
      return of(new authActions.LoginFailed())
    })
  );

  @Effect()
  register: Observable<Action> = this.actions$.pipe(ofType(AuthActionTypes.TRY_REGISTER),
    mergeMap((actionInput: TryRegister) => this.authService.register(actionInput.user).pipe(
      map((response: Response) => {
        this.router.navigate(['/auth/login']);
        return new authActions.RegisterSuccess()
      })
    )), catchError((response: HttpErrorResponse) => {
      this.alertService.errorAlert(response.error);
      setTimeout(function() {
        window.location.reload();
      }, 1500);

      return of(new authActions.RegisterFail())
    }))
}
