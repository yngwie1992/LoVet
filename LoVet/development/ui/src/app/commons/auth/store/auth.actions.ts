import {Action} from "@ngrx/store";
import {User} from "../model/user";


export enum AuthActionTypes {
  TRY_LOG_IN = '[auth] try to log in',
  LOG_IN_SUCCESS = '[auth] log in success',
  LOG_IN_FAIL = '[auth] log in failed',

  LOG_OUT = '[auth] log out',

  TRY_REGISTER = '[auth] try to register',
  REGISTER_SUCCESS = '[auth] register success',
  REGISTER_FAIL = '[auth] register failed'
}

export class Logout implements Action {
  readonly type = AuthActionTypes.LOG_OUT;
}

export class TryRegister implements Action {
  readonly type = AuthActionTypes.TRY_REGISTER;
  constructor(public user: User) {
  }
}

export class RegisterSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_SUCCESS;
}

export class RegisterFail implements Action {
  readonly type = AuthActionTypes.REGISTER_FAIL;
}

export class TryLogIn implements Action {
  readonly type = AuthActionTypes.TRY_LOG_IN;
  constructor(public user: User) {
  }
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LOG_IN_SUCCESS;
  constructor(public username: string, public role: string) {
  }
}

export class LoginFailed implements Action {
  readonly type = AuthActionTypes.LOG_IN_FAIL;
}

export type AuthActions =
  TryLogIn |
  LoginSuccess |
  LoginFailed |
  TryRegister |
  RegisterSuccess |
  RegisterFail |
  Logout
