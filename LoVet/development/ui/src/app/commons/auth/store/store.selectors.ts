import {createFeatureSelector, createSelector} from "@ngrx/store";
import {AuthState} from "./auth.reducers";

export const getAuthFeatureState = createFeatureSelector<AuthState>(
  "auth"
);

export const getIsLoggedIn = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state.isLoggedIn
);

export const getIsLoggedFailed = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state.isLoginFailed
);

export const getUsername = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state.username
);

export const getRole = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state.role
);
