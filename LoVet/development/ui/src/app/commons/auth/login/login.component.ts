import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../app.reducer";
import * as authActions from "../store/auth.actions"
import {User} from "../model/user";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  isLoginFailed = false;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    //this.store.select('auth').subscribe(result => this.isLoginFailed = result.isLoginFailed)
  }

  onLoginClick() {
    this.store.dispatch(new authActions.TryLogIn(new User(this.username, this.password)));
  }
}
