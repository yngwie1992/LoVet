import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Store} from "@ngrx/store";
import * as authActions from "../store/auth.actions"
import {AuthState} from "../store/auth.reducers";
import {User} from "../model/user";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @ViewChild('form') signupForm: NgForm;

  constructor(private store: Store<AuthState>) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.store.dispatch(new authActions.TryRegister(
      new User(this.signupForm.value.email
        , this.signupForm.value.password
        , this.signupForm.value.phoneNumber
        , this.signupForm.value.repeatpassword)));
  }
}
