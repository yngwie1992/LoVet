import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AuthState} from "../../auth/store/auth.reducers";
import {Observable} from "rxjs";
import * as authActions from "../../auth/store/auth.actions"

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  authState: Observable<AuthState>;

  constructor(private store: Store<AuthState>) { }

  ngOnInit() {
    this.authState = this.store.select('auth')
  }

  onLogoutClick() {
    this.store.dispatch(new authActions.Logout());
  }
}
