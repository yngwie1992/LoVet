import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {Store} from "@ngrx/store";
import {AuthState} from "../auth/store/auth.reducers";
import * as authSelector from "../auth/store/store.selectors";

@Injectable()
export class OwnerAuthGuardService implements CanActivate {

  role = '';

  constructor(private store: Store<AuthState>){
  }

  canActivate(): boolean {
    this.store.select(authSelector.getRole).subscribe((userRole: string) => this.role = userRole);

    console.log(this.role + " ROLE_PET_OWNER");

    return this.role === 'ROLE_PET_OWNER';
  }

}
