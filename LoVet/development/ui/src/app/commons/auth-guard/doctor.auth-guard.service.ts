import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {AuthState} from "../auth/store/auth.reducers";
import {Store} from "@ngrx/store";
import * as authSelector from "../auth/store/store.selectors"

@Injectable()
export class DoctorAuthGuardService implements CanActivate {

  role = '';

  constructor(private store: Store<AuthState>){
  }

  canActivate(): boolean {
    this.store.select(authSelector.getRole).subscribe((userRole: string) => this.role = userRole);

    console.log(this.role + " ROLE_DOCTOR");

    return this.role === 'ROLE_DOCTOR';
  }

}
