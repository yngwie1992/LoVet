import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {Observable, of} from "rxjs";
import {Action} from "@ngrx/store";
import * as doctorActions from "../../doctor/store/doctor.actions";
import {catchError, map, mergeMap} from "rxjs/operators";
import {DoctorService} from "../service/doctor.service";
import {DoctorActionTypes, TryLoadAvailableDoctorsByDateTime} from "./doctor.actions";
import {Doctor} from "../model/doctor";

@Injectable()
export class DoctorEffects {

  constructor(private actions$: Actions, private doctorService: DoctorService) {
  }

  @Effect()
  loadAllDoctors: Observable<Action> = this.actions$.pipe(ofType(DoctorActionTypes.TRY_LOAD_DOCTORS),
    mergeMap(() => this.doctorService.loadAll().pipe(
      map((doctors: Doctor[]) => {
        return new doctorActions.LoadDoctorsSuccess(doctors)
      })
    )), catchError(error => {
      console.log("load doctors failed" + error);
      return of(null)
    }));

  @Effect()
  loadAvailableDoctorsByDateTime: Observable<Action> = this.actions$.pipe(ofType(DoctorActionTypes.TRY_LOAD_AVAILABLE_DOCTORS_BY_DATE_TIME),
    mergeMap((actionInput: TryLoadAvailableDoctorsByDateTime ) => this.doctorService.loadAvailableOn(actionInput.date).pipe(
      map((doctors: Doctor[]) => {
        return new doctorActions.LoadAvailableDoctorsByDateSuccess(doctors)
      })
    )), catchError(error => {
      console.log("load doctors BY DATE failed" + error);
      return of(null)
    }));


}
