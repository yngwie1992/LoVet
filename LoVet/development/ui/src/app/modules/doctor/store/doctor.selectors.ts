import {createFeatureSelector, createSelector} from "@ngrx/store";
import {DoctorState} from "./doctor.reducers";

const getDoctorFeatureState = createFeatureSelector<DoctorState>(
  "doctor"
);

export const getDoctors = createSelector(
  getDoctorFeatureState,
  (state: DoctorState) => state.doctors
);
