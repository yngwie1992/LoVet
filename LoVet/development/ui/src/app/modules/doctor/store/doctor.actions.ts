import {Action} from "@ngrx/store";
import {Doctor} from "../model/doctor";

export enum DoctorActionTypes {
  TRY_LOAD_DOCTORS = '[doctor] try load doctors',
  LOAD_DOCTORS_SUCCESS = '[doctor] load doctors success',

  TRY_LOAD_AVAILABLE_DOCTORS_BY_DATE_TIME = '[doctor] try load doctors by datetime',
  LOAD_AVAILABLE_DOCTORS_BY_DATE_TIME_SUCCESS = '[doctor] load doctors by time success',

  CLEAN_DOCTORS = '[doctor] clean doctors'
}

export class LoadAvailableDoctorsByDateSuccess implements Action {
  readonly type = DoctorActionTypes.LOAD_AVAILABLE_DOCTORS_BY_DATE_TIME_SUCCESS;
  constructor(public doctors: Doctor[]) {
  }
}

export class CleanDoctors implements Action{
  readonly type = DoctorActionTypes.CLEAN_DOCTORS;
}

export class TryLoadAvailableDoctorsByDateTime implements Action {
  readonly type = DoctorActionTypes.TRY_LOAD_AVAILABLE_DOCTORS_BY_DATE_TIME;

  constructor(public date: string) {
  }
}

export class TryLoadDoctors implements Action {
  readonly type = DoctorActionTypes.TRY_LOAD_DOCTORS;
}

export class LoadDoctorsSuccess implements Action {
  readonly type = DoctorActionTypes.LOAD_DOCTORS_SUCCESS;

  constructor(public doctors: Doctor[]) {
  }
}

export type DoctorActions =
  LoadDoctorsSuccess |
  TryLoadAvailableDoctorsByDateTime |
  LoadAvailableDoctorsByDateSuccess |
  CleanDoctors
