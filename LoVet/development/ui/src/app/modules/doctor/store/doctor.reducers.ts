import {Doctor} from "../model/doctor";
import {DoctorActions, DoctorActionTypes} from "./doctor.actions";

export interface DoctorState {
  doctors: Doctor[]
}

export const initState: DoctorState = {
  doctors: []
};

export function doctorReducer(state = initState, action: DoctorActions) {
  switch (action.type) {
    case DoctorActionTypes.CLEAN_DOCTORS: {
      return {
        ...state,
        doctors: []
      }
    }

    case DoctorActionTypes.LOAD_AVAILABLE_DOCTORS_BY_DATE_TIME_SUCCESS: {
      return {
        ...state,
        doctors: action.doctors
      }
    }
    case DoctorActionTypes.LOAD_DOCTORS_SUCCESS: {
      return {
        ...state,
        doctors: action.doctors
      }
    }
    default:
      return state
  }
}
