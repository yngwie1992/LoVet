import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as doctorSelectors from "../store/doctor.selectors"
import * as doctorActions from "../store/doctor.actions"
import {Doctor} from "../model/doctor";
import {AppState} from "../../../app.reducer";
import {Location} from "@angular/common";

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent implements OnInit {

  doctors$: Observable<Doctor[]>;

  constructor(private store: Store<AppState>, private location: Location) { }

  ngOnInit() {
    this.store.dispatch(new doctorActions.TryLoadDoctors());
    this.doctors$ = this.store.select(doctorSelectors.getDoctors)
  }

  onBackClick() {
    this.location.back();
  }
}
