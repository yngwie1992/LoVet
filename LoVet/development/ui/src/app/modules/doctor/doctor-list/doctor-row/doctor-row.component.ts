import {Component, Input, OnInit} from '@angular/core';
import {Doctor} from "../../model/doctor";

@Component({
  selector: 'tr[app-doctor-row]',
  templateUrl: './doctor-row.component.html',
  styleUrls: ['./doctor-row.component.css']
})
export class DoctorRowComponent implements OnInit {

  @Input()
  doctor: Doctor;

  constructor() { }

  ngOnInit() {
  }

}
