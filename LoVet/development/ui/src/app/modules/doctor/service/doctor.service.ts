import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Doctor} from "../model/doctor";

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  BASE_URL = '/api/doctors';

  constructor(private http: HttpClient) { }

  loadAll(): Observable<Doctor[]> {
    return this.http.get<Doctor[]>(this.BASE_URL);
  }

  loadAvailableOn(date: string) {
    return this.http.get(this.BASE_URL + '/available/' + date);
  }
}
