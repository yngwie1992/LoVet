import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {DoctorListComponent} from "./doctor-list/doctor-list.component";
import {DoctorComponent} from "./doctor/doctor.component";

const doctorRoutes: Routes = [
  {path: 'doctors', component: DoctorListComponent},
  {path: 'doctor', component: DoctorComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(doctorRoutes)
  ],
  exports: [RouterModule]
})
export class DoctorRoutingModule {

}
