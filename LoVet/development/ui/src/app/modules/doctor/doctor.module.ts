import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DoctorRoutingModule} from "./doctor-routing.module";
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctorComponent } from './doctor/doctor.component';
import { DoctorRowComponent } from './doctor-list/doctor-row/doctor-row.component';

@NgModule({
  declarations: [DoctorListComponent, DoctorComponent, DoctorRowComponent],
  imports: [
    CommonModule,
    DoctorRoutingModule
  ]
})
export class DoctorModule {
}
