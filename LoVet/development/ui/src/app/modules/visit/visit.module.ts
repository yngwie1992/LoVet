import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VisitListComponent} from './visit-list/visit-list.component';
import {VisitComponent} from './visit/visit.component';
import {VisitRoutingModule} from "./visit-routing.module";
import {CalendarComponent} from "../../commons/calendar/calendar.component";
import {VisitBookComponent} from "./visit-book/visit-book.component";
import {CalendarDayComponent} from "../../commons/calendar/calendar-day/calendar-day.component";
import {VisitBookConfirmComponent} from './visit-book/visit-book-confirm/visit-book-confirm.component';
import {VisitRowComponent} from "./visit-list/visit-row/visit-row.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [VisitListComponent, VisitRowComponent, VisitComponent, VisitBookComponent, CalendarComponent, CalendarDayComponent, VisitBookConfirmComponent],
  imports: [
    CommonModule,
    VisitRoutingModule,
    FormsModule,
  ],
})
export class VisitModule {
}
