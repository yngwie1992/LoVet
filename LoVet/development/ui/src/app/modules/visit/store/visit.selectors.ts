import {createFeatureSelector, createSelector} from "@ngrx/store";
import {VisitState} from "./visit.reducers";

const getVisitFeatureState = createFeatureSelector<VisitState>(
  "visit"
);

export const getVisits = createSelector(
  getVisitFeatureState,
  (state: VisitState) => state.visits
);

export const getBookingDate = createSelector(
  getVisitFeatureState,
  (state: VisitState) => state.date
);
