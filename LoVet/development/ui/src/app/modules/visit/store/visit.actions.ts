import {Action} from "@ngrx/store";
import {Visit} from "../model/visit";

export enum VisitActionTypes {
  TRY_LOAD_PET_VISITS_BY_PET_ID = '[visit] try load pet visits by pet id',
  LOAD_PET_VISITS_BY_PET_ID_SUCCESS = '[visit] load pet visits by pet id success',

  TRY_LOAD_PET_VISITS_BY_DAY = '[visit] try load pet visits by DAY',
  LOAD_PET_VISITS_BY_DAY_SUCCESS = '[visit] load pet visits by DAY success',

  STORE_BOOKING_DATE = '[visit] store booking date',

  TRY_SAVE_VISIT = '[visit] try save visit',
  SAVE_VISIT_SUCCESS = '[visit] save visit success',

  TRY_BOOK_VISIT = '[visit] try book visit',
  BOOK_VISIT_SUCCESS = '[visit] book visit success',
}

//TODO
//store for booking and saving visit
//validate availbility on backend

export class TryBookVisit implements Action {
  readonly type = VisitActionTypes.TRY_BOOK_VISIT;
  constructor(public visit: Visit) {
  }
}

export class BookVisitSuccess implements Action {
  readonly type = VisitActionTypes.BOOK_VISIT_SUCCESS;
}

export class StoreBookingDate implements Action {
  readonly type = VisitActionTypes.STORE_BOOKING_DATE;
  constructor(public date: string) {
  }
}

export class TryLoadPetVisitsByDay implements Action {
  readonly type = VisitActionTypes.TRY_LOAD_PET_VISITS_BY_DAY;
  constructor(public date: Date) {
  }
}

export class LoadPetVisitsByDaySuccess implements Action {
  readonly type = VisitActionTypes.LOAD_PET_VISITS_BY_DAY_SUCCESS;
  constructor(public visits: Visit[]) {
  }
}

export class TryLoadPetVisitsByPetId implements Action {
  readonly type = VisitActionTypes.TRY_LOAD_PET_VISITS_BY_PET_ID;

  constructor(public petId: number) {
  }
}

export class LoadPetVisitsByPetIdSuccess implements Action {
  readonly type = VisitActionTypes.LOAD_PET_VISITS_BY_PET_ID_SUCCESS;

  constructor(public visits: Visit[]) {
  }
}


export type VisitActions =
  TryLoadPetVisitsByPetId |
  LoadPetVisitsByPetIdSuccess |
  TryLoadPetVisitsByDay |
  LoadPetVisitsByDaySuccess |
  StoreBookingDate |
  BookVisitSuccess
