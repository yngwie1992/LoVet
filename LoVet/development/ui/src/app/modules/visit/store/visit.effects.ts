import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {Observable, of} from "rxjs";
import {Action} from "@ngrx/store";
import * as visitActions from "./visit.actions";
import {catchError, map, mergeMap} from "rxjs/operators";
import {VisitService} from "../service/visit.service";
import {TryBookVisit, TryLoadPetVisitsByDay, TryLoadPetVisitsByPetId, VisitActionTypes} from "./visit.actions";
import {Visit} from "../model/visit";
import {AlertService} from "../../../commons/alert/alert.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";


@Injectable()
export class VisitEffects {

  constructor(private actions$: Actions, private visitService: VisitService, private alertService: AlertService, private router: Router) {
  }

  @Effect()
  loadVisitsByPetId: Observable<Action> = this.actions$.pipe(ofType(VisitActionTypes.TRY_LOAD_PET_VISITS_BY_PET_ID),
    mergeMap((actionInput: TryLoadPetVisitsByPetId) => this.visitService.loadAllByPetId(actionInput.petId).pipe(
      map((visits: Visit[]) => {
        return new visitActions.LoadPetVisitsByPetIdSuccess(visits)
      }, catchError((response: HttpErrorResponse) => {
        this.alertService.errorAlert(response.error);
        setTimeout(function () {
          this.router.navigate(['/'])
        }, 1500);
        return of(null) //loading failed action
      }))
    )));

  @Effect()
  loadVisitsByDay: Observable<Action> = this.actions$.pipe(ofType(VisitActionTypes.TRY_LOAD_PET_VISITS_BY_DAY),
    mergeMap((actionInput: TryLoadPetVisitsByDay) => this.visitService.loadByDate(actionInput.date).pipe(
      map((visits: Visit[]) => {
        return new visitActions.LoadPetVisitsByDaySuccess(visits)
      }, catchError((response: HttpErrorResponse) => {
        this.alertService.errorAlert(response.error);
        setTimeout(function () {
          this.router.navigate(['/'])
        }, 1500);
        return of(null) //loading failed action
      }))
    )));

  @Effect()
  bookVisit: Observable<Action> = this.actions$.pipe(ofType(VisitActionTypes.TRY_BOOK_VISIT),
    mergeMap((actionInput: TryBookVisit) => this.visitService.bookVisit(actionInput.visit).pipe(
      map(() => {
        this.router.navigate(['/visit/book']);
        return new visitActions.BookVisitSuccess()
      }, catchError((response: HttpErrorResponse) => {
        this.alertService.errorAlert(response.error);
        setTimeout(function () {
          this.router.navigate(['/'])
        }, 1500);
        return of(null) //loading failed action
      }))
    )));
}
