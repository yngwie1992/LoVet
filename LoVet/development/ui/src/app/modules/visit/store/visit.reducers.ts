import {Visit} from "../model/visit";
import {VisitActions, VisitActionTypes} from "./visit.actions";

export interface VisitState {
  visits: Visit[]
  date: string
}


export const initState: VisitState = {
  visits: [],
  date: undefined
};

export function visitReducer(state = initState, action: VisitActions) {
  switch(action.type) {
    case VisitActionTypes.BOOK_VISIT_SUCCESS: {
      return {
        ...state,
        date: undefined
      }
    }
    case VisitActionTypes.STORE_BOOKING_DATE: {
      return {
        ...state,
        date: action.date
      }
    }
    case VisitActionTypes.LOAD_PET_VISITS_BY_PET_ID_SUCCESS: {
      return {
        ...state,
        visits: action.visits
      }
    }
    case VisitActionTypes.LOAD_PET_VISITS_BY_DAY_SUCCESS: {
      return {
        ...state,
        visits: action.visits
      }
    }

    default:
      return {
        ...state
      }
  }
}
