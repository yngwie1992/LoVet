import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitRowComponent } from './visit-row.component';

describe('VisitRowComponent', () => {
  let component: VisitRowComponent;
  let fixture: ComponentFixture<VisitRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
