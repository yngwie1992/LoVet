import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AppState} from "../../../app.reducer";
import {Store} from "@ngrx/store";
import * as visitActions from "../store/visit.actions"
import * as visitSelectors from "../store/visit.selectors"
import {Observable} from "rxjs";
import {Location} from '@angular/common';
import {Visit} from "../model/visit";

@Component({
  selector: 'app-visit-list',
  templateUrl: './visit-list.component.html',
  styleUrls: ['./visit-list.component.css']
})
export class VisitListComponent implements OnInit {

  visits$: Observable<Visit[]>;

  constructor(private activeRoute: ActivatedRoute, private store: Store<AppState>, private location: Location) {
  }

  ngOnInit() {
    let petId = this.activeRoute.snapshot.paramMap.get("id");

    this.store.dispatch(new visitActions.TryLoadPetVisitsByPetId(Number(petId)));

    this.visits$ = this.store.select(visitSelectors.getVisits);
  }

  onBackClick() {
    this.location.back();
  }
}
