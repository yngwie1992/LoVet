import {Component, Input, OnInit} from '@angular/core';
import {Visit} from "../../model/visit";

@Component({
  selector: 'tr[app-visit-row]',
  templateUrl: './visit-row.component.html',
  styleUrls: ['./visit-row.component.css']
})
export class VisitRowComponent implements OnInit {

  @Input()
  visit: Visit;

  @Input()
  iterator: number;

  constructor() { }

  ngOnInit() {
  }

}
