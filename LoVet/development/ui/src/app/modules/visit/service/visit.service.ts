import {Injectable} from '@angular/core';
import {Visit} from "../model/visit";
import {HttpClient} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class VisitService {

  constructor(private http: HttpClient) {
  }


  save(visit: Visit) {
    this.http.post('/api/visits', visit);
  }

  loadAllByPetId(id: number) {
    return this.http.get('/api/visits/' + id);
  }

  loadById(id: number) {
    return this.http.get('/api/visits/visit/' + id);
  }

  loadByDate(date: Date) {
    return this.http.get('/api/visits/visit/date/' + this.convertDateToString(date))
  }

  bookVisit(visit: Visit) {
    return this.http.post('/api/visits/book', visit);
  }

  convertDateToString(date: Date): string {
    return date.getFullYear() + "-" + (Number(date.getMonth()) + 1) + "-" + (Number(date.getUTCDate()) + 1);
  }
}


