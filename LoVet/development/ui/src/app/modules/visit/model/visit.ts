export class Visit {

  public id: number;
  public petName: string;
  public doctorLastName: string;

  constructor(public petId: number, public date: string, public doctorId: string) {
  }
}
