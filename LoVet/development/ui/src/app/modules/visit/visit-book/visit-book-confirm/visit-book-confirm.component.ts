import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppState} from "../../../../app.reducer";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as petActions from "../../../pet/store/pet.actions"
import * as doctorActions from "../../../doctor/store/doctor.actions"
import * as petSelectors from "../../../pet/store/pet.selectors"
import * as visitActions from "../../store/visit.actions"
import * as visitSelector from "../../store/visit.selectors"
import * as authSelector from "../../../../commons/auth/store/store.selectors"
import {Visit} from "../../model/visit";
import {Pet} from "../../../pet/model/pet";
import * as doctorSelectors from "../../../doctor/store/doctor.selectors"
import {Doctor} from "../../../doctor/model/doctor";

@Component({
  selector: 'app-visit-book-confirm',
  templateUrl: './visit-book-confirm.component.html',
  styleUrls: ['./visit-book-confirm.component.css'],
  providers: []
})
export class VisitBookConfirmComponent implements OnInit, OnDestroy {

  pets$: Observable<Pet[]>;
  bookingDate$: Observable<string>;
  doctors$: Observable<Doctor[]>;
  bookingDateString: string;
  loggedUser: string;
  selectedPetId: number;
  selectedDoctorId: string;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.bookingDate$ = this.store.select(visitSelector.getBookingDate);
    this.bookingDate$.subscribe((date: string) => this.bookingDateString = date);

    this.store.select(authSelector.getUsername).subscribe((username: string) => this.loggedUser = username);
    this.store.dispatch(new petActions.TryLoadOwnerPets(this.loggedUser));
    this.pets$ = this.store.select(petSelectors.getPets);

    this.store.dispatch(new doctorActions.TryLoadAvailableDoctorsByDateTime(this.bookingDateString));
    this.doctors$ = this.store.select(doctorSelectors.getDoctors);
  }

  onBookVisitConfirm() {
    this.store.dispatch(new visitActions.TryBookVisit(this.prepareVisit(this.bookingDateString)))
  }

  private prepareVisit(date: string) {
    return new Visit(this.selectedPetId, date, this.selectedDoctorId);
  }

  ngOnDestroy(): void {
    this.store.dispatch(new doctorActions.CleanDoctors())
  }
}
