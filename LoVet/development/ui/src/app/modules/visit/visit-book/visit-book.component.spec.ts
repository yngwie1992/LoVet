import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitBookComponent } from './visit-book.component';

describe('VisitBookComponent', () => {
  let component: VisitBookComponent;
  let fixture: ComponentFixture<VisitBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
