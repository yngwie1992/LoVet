import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitBookConfirmComponent } from './visit-book-confirm.component';

describe('VisitBookConfirmComponent', () => {
  let component: VisitBookConfirmComponent;
  let fixture: ComponentFixture<VisitBookConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitBookConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitBookConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
