import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {VisitListComponent} from "./visit-list/visit-list.component";
import {VisitComponent} from "./visit/visit.component";
import {VisitBookComponent} from "./visit-book/visit-book.component";
import {VisitBookConfirmComponent} from "./visit-book/visit-book-confirm/visit-book-confirm.component";

const visitRoutes: Routes = [
  {path: 'visits', component: VisitListComponent},
  {path: 'visits/:id', component: VisitListComponent},
  {path: 'visit', component: VisitComponent},
  {path: 'book', component: VisitBookComponent},
  {path: 'book/confirm', component: VisitBookConfirmComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(visitRoutes)
  ],
  exports: [RouterModule]
})
export class VisitRoutingModule {

}
