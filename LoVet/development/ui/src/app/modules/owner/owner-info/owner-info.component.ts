import {Component, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {AppState} from "../../../app.reducer";
import * as ownerActions from "../store/owner.actions"
import {Observable} from "rxjs";
import * as ownerSelectors from "../store/owner.selectors"
import * as authSelectors from "../../../commons/auth/store/store.selectors"
import {Owner} from "../model/owner";

@Component({
  selector: 'app-owner-info',
  templateUrl: './owner-info.component.html',
  styleUrls: ['./owner-info.component.css']
})
export class OwnerInfoComponent implements OnInit {

  currentLoggedOwner: string;
  owner$: Observable<Owner>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.select(authSelectors.getUsername).subscribe((username: string) => this.currentLoggedOwner = username);
    this.store.dispatch(new ownerActions.TryLoadOwner(this.currentLoggedOwner));
    this.owner$ = this.store.select(ownerSelectors.getOwner);
  }

}
