import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  constructor(private http: HttpClient) { }

  loadAll() {
    return this.http.get("/api/owners");
  }

  loadByOwnerUsername(username: string) {
    return this.http.get("api/owners/username/" + username);
  }
}
