import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {DoctorAuthGuardService} from "../../commons/auth-guard/doctor.auth-guard.service";
import {OwnerListComponent} from "./owner-list/owner-list.component";
import {OwnerRowComponent} from "./owner-list/owner-row/owner-row.component";
import {OwnerInfoComponent} from "./owner-info/owner-info.component";

const ownerRoutes: Routes = [
  {path: 'owners', component: OwnerListComponent, canActivate: [DoctorAuthGuardService] },
  {path: 'owner', component: OwnerRowComponent},
  {path: 'info', component: OwnerInfoComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(ownerRoutes)
  ],
  exports: [RouterModule]
})
export class OwnerRoutingModule {

}
