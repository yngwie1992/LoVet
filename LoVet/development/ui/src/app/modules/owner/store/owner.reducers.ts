import {Owner} from "../model/owner";
import {OwnerActions, OwnerActionTypes} from "./owner.actions";

export interface OwnerState {
  owners: Owner[]
  owner: Owner
}

export const initState: OwnerState = {
  owners: [],
  owner: null
};

export function ownerReducer(state = initState, action: OwnerActions) {
  switch (action.type) {
    case OwnerActionTypes.LOAD_ALL_OWNERS_SUCCESS: {
      return {
        ...state,
        owners: action.owners
      }
    }
    case OwnerActionTypes.LOAD_OWNER_SUCCESS: {
      return {
        ...state,
        owner: action.owner
      }
    }

    default:
      return state
  }
}
