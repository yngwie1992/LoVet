import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {Observable, of} from "rxjs";
import {Action} from "@ngrx/store";
import * as ownerActions from "../../owner/store/owner.actions";
import {OwnerActionTypes, TryLoadOwner} from "./owner.actions";
import {catchError, map, mergeMap} from "rxjs/operators";
import {OwnerService} from "../service/owner.service";
import {Owner} from "../model/owner";


@Injectable()
export class OwnersEffects {

  constructor(private actions$: Actions, private ownerService: OwnerService) {
  }

  @Effect()
  loadAllOwners: Observable<Action> = this.actions$.pipe(ofType(OwnerActionTypes.TRY_LOAD_ALL_OWNERS),
    mergeMap(() => this.ownerService.loadAll().pipe(
      map((owners: Owner[]) => {
        return new ownerActions.LoadOwnersSuccess(owners)
      }, catchError(err => {
        console.log("ERR1" + err);
        return of(null)
      }))
    )), catchError(error => {
      console.log("ERR2" + error);
      return of(null)
    }));

  @Effect()
  loadOwner: Observable<Action> = this.actions$.pipe(ofType(OwnerActionTypes.TRY_LOAD_OWNER),
    mergeMap((actionInput: TryLoadOwner) => this.ownerService.loadByOwnerUsername(actionInput.username).pipe(
      map((owner: Owner) => {
        return new ownerActions.LoadOwnerSuccess(owner)
      }, catchError(err => {
        console.log("ERR1" + err);
        return of(null)
      }))
    )), catchError(error => {
      console.log("ERR2" + error);
      return of(null)
    }))
}
