import {Action} from "@ngrx/store";
import {Owner} from "../model/owner";

export enum OwnerActionTypes {
  TRY_LOAD_ALL_OWNERS = '[owner] try load owners',
  LOAD_ALL_OWNERS_SUCCESS = '[owner] load owners succeed',

  TRY_LOAD_OWNER = '[owner] try load owner',
  LOAD_OWNER_SUCCESS = '[owner] load One owner success',
}

export class TryLoadOwner implements Action {
  readonly type = OwnerActionTypes.TRY_LOAD_OWNER;
  constructor(public username: string) {
  }
}

export class LoadOwnerSuccess implements Action {
  readonly type = OwnerActionTypes.LOAD_OWNER_SUCCESS;
  constructor(public owner: Owner) {
  }
}

export class TryLoadOwners implements Action {
  readonly type = OwnerActionTypes.TRY_LOAD_ALL_OWNERS;
}

export class LoadOwnersSuccess implements Action {
  readonly type = OwnerActionTypes.LOAD_ALL_OWNERS_SUCCESS;
  constructor(public owners: Owner[]) {
  }
}

export type OwnerActions =
  TryLoadOwners |
  LoadOwnersSuccess |
  TryLoadOwner |
  LoadOwnerSuccess
