import {createFeatureSelector, createSelector} from "@ngrx/store";
import {OwnerState} from "./owner.reducers";


const getOwnerFeatureState = createFeatureSelector<OwnerState>(
  "owner"
);

export const getOwner = createSelector(
  getOwnerFeatureState,
  (state: OwnerState) => state.owner
);

export const getOwners = createSelector(
  getOwnerFeatureState,
  (state: OwnerState) => state.owners
);
