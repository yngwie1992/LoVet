export class Owner {

  constructor(public name: string, public surname: string, public phoneNumber: string, public email: string) {
  }
}
