import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as ownerActions from "../../owner/store/owner.actions";
import * as ownerSelectors from "../../owner/store/owner.selectors";
import {Router} from "@angular/router";
import {AppState} from "../../../app.reducer";
import {Owner} from "../model/owner";

@Component({
  selector: 'app-owner-list',
  templateUrl: './owner-list.component.html',
  styleUrls: ['./owner-list.component.css']
})
export class OwnerListComponent implements OnInit {

  owners$: Observable<Owner[]>;

  constructor(private store: Store<AppState>, private router: Router) {
    this.store.dispatch(new ownerActions.TryLoadOwners());
    this.owners$ = store.pipe(select(ownerSelectors.getOwners));
  }

  ngOnInit() {
  }

  onOwnerClick(username: string) {
    this.router.navigate(['/pet/pets/' + username])
  }
}
