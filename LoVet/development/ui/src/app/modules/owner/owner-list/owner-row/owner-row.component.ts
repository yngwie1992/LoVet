import {Component, Input, OnInit} from '@angular/core';
import {Owner} from "../../model/owner";

@Component({
  selector: 'tr[app-owner-row]',
  templateUrl: './owner-row.component.html',
  styleUrls: ['./owner-row.component.css']
})
export class OwnerRowComponent implements OnInit {

  @Input()
  owner: Owner;
  @Input()
  iterator: number;

  constructor() { }

  ngOnInit() {
  }

}
