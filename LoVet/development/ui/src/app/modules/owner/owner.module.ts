import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnerListComponent } from './owner-list/owner-list.component';
import {OwnerRoutingModule} from "./owner-routing.module";
import {OwnerRowComponent} from "./owner-list/owner-row/owner-row.component";
import { OwnerInfoComponent } from './owner-info/owner-info.component';
import {PetListComponent} from "../pet/pet-list/pet-list.component";
import {PetModule} from "../pet/pet.module";

@NgModule({
  declarations: [OwnerRowComponent, OwnerListComponent, OwnerInfoComponent],
  imports: [
    CommonModule,
    OwnerRoutingModule,
    PetModule
  ],
  exports: [OwnerListComponent]
})
export class OwnerModule { }
