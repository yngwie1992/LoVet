import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {PetListComponent} from "./pet-list/pet-list.component";

const petRoutes: Routes = [
  {path: 'pets/:username', component: PetListComponent},
  {path: 'pet', component: PetListComponent} //TODO petcomponent to show particular pet
];

@NgModule({
  imports: [
    RouterModule.forChild(petRoutes)
  ],
  exports: [RouterModule]
})
export class PetRoutingModule {

}
