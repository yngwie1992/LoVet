export class Pet {

  public id: number;

  constructor(public name: string, public petType: string, public dateOfBirth: Date) {
  }
}
