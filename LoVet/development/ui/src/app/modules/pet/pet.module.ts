import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PetRoutingModule} from "./pet-routing.module";
import { PetListComponent } from './pet-list/pet-list.component';
import { PetRowComponent } from './pet-list/pet-row/pet-row.component';

@NgModule({
  declarations: [PetListComponent, PetRowComponent],
  exports: [
    PetListComponent
  ],
  imports: [
    CommonModule,
    PetRoutingModule
  ]
})
export class PetModule { }
