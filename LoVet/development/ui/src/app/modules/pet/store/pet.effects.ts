import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {PetService} from "../service/pet.service";
import {Observable, of} from "rxjs";
import {Action} from "@ngrx/store";
import * as petActions from "../../pet/store/pet.actions";
import {catchError, map, mergeMap} from "rxjs/operators";
import {PetActionTypes, TryLoadOwnerPets} from "./pet.actions";
import {Pet} from "../model/pet";
import {HttpErrorResponse} from "@angular/common/http";
import {AlertService} from "../../../commons/alert/alert.service";

@Injectable()
export class PetEffects {

  constructor(private actions$: Actions, private petService: PetService, private alertService: AlertService) {
  }

  @Effect()
  loadPets: Observable<Action> = this.actions$.pipe(ofType(PetActionTypes.TRY_LOAD_OWNER_PETS),
    mergeMap((actionInput: TryLoadOwnerPets) => this.petService.loadAllByUsername(actionInput.username).pipe(
      map((pets: Pet[]) => {
        return new petActions.LoadOwnerPetsSuccess(pets)
      }, catchError((response: HttpErrorResponse) => {
        this.alertService.errorAlert(response.error);
        setTimeout(function () {
          this.router.navigate(['/'])
        }, 1500);
        return of(null) //load pets failed
      }))
    )));
}
