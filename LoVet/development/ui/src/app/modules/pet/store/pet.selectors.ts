import {createFeatureSelector, createSelector} from "@ngrx/store";
import {PetState} from "./pet.reducers";


export const getPetFeatureState = createFeatureSelector<PetState>(
  "pet"
);

export const getPets = createSelector(
  getPetFeatureState,
  (state: PetState) => state.pets
);
