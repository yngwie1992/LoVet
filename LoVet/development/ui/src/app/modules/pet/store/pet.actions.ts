import {Action} from "@ngrx/store";
import {Pet} from "../model/pet";

export enum PetActionTypes {
  TRY_LOAD_OWNER_PETS = '[pet] try load owner pets',
  LOAD_OWNER_PETS_SUCCESS = '[pet] loading pets succeed',

  TRY_LOAD_ALL_PETS = '[pet] try load all pets',
  LOAD_OWNER_ALL_SUCCESS = '[pet] loading all pets succeed',
}


export class TryLoadOwnerPets implements Action {
  readonly type = PetActionTypes.TRY_LOAD_OWNER_PETS;
  constructor(public username: string) {
  }
}

export class LoadOwnerPetsSuccess implements Action {
  readonly type = PetActionTypes.LOAD_OWNER_PETS_SUCCESS;

  constructor(public pets: Pet[]) {
  }
}

export type PetActions =
  TryLoadOwnerPets |
  LoadOwnerPetsSuccess
