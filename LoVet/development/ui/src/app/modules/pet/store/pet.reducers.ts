import {Pet} from "../model/pet";
import {PetActions, PetActionTypes} from "./pet.actions";

export interface PetState {
  pets: Pet[]
}

export const initState: PetState = {
  pets: []
};


export function petReducer(state = initState, action: PetActions) {
  switch (action.type) {
    case(PetActionTypes.LOAD_OWNER_PETS_SUCCESS): {
      return {
        ...state,
        pets: action.pets
      }
    }

    default:
      return state
  }
}
