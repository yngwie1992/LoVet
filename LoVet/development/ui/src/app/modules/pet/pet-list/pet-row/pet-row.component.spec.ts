import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetRowComponent } from './pet-row.component';

describe('PetRowComponent', () => {
  let component: PetRowComponent;
  let fixture: ComponentFixture<PetRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
