import {Component, OnInit} from '@angular/core';
import {PetState} from "../store/pet.reducers";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import * as petActions from "../store/pet.actions";
import * as petSelectors from "../store/pet.selectors";
import {Location} from '@angular/common';
import {Pet} from "../model/pet";

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.css']
})
export class PetListComponent implements OnInit {

  pets$: Observable<Pet[]>;

  constructor(private store: Store<PetState>, private activeRoute: ActivatedRoute, private location: Location, private router: Router) { }

  ngOnInit() {
    let pathUsername = this.activeRoute.snapshot.paramMap.get("username");
    this.store.dispatch(new petActions.TryLoadOwnerPets(pathUsername));
    this.pets$ = this.store.select(petSelectors.getPets);
  }

  onBackClick() {
    this.location.back();
  }

  onPetClick(petId: number) {
    this.router.navigate(['visit/visits/' + petId]);
  }
}
