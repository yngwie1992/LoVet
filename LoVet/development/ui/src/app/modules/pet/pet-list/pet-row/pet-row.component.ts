import {Component, Input, OnInit} from '@angular/core';
import {Pet} from "../../model/pet";

@Component({
  selector: 'tr[app-pet-row]',
  templateUrl: './pet-row.component.html',
  styleUrls: ['./pet-row.component.css']
})
export class PetRowComponent implements OnInit {

  @Input()
  pet: Pet;

  @Input()
  iterator: number;

  constructor() { }

  ngOnInit() {
  }

}
