import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PetService {
  constructor(private http: HttpClient) { }

  loadAllByUsername(username: string) {
    return this.http.get('/api/pets/' + username);
  }

  loadAll() {
    return this.http.get('/api/pets');
  }
}
