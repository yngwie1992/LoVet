import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./commons/home/home/home.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'auth', loadChildren: './commons/auth/auth.module#AuthModule'},
  {path: 'doctor', loadChildren: './modules/doctor/doctor.module#DoctorModule'},
  {path: 'owner', loadChildren: './modules/owner/owner.module#OwnerModule'},
  {path: 'pet', loadChildren: './modules/pet/pet.module#PetModule'},
  {path: 'visit', loadChildren: './modules/visit/visit.module#VisitModule'}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
