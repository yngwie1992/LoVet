package com.lovet.doctors.usecases;

import com.lovet.EntityId;

public interface DeleteDoctors {

    void deleteById(EntityId id);
}
