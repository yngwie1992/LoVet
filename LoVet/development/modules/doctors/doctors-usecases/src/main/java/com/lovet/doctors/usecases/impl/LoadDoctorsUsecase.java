package com.lovet.doctors.usecases.impl;

import com.lovet.EntityId;
import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.usecases.LoadDoctors;
import com.lovet.doctors.usecases.gateways.DoctorQueryGateway;
import com.lovet.visits.domain.Visit;
import com.lovet.visits.usecases.gateways.VisitQueryGateway;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class LoadDoctorsUsecase implements LoadDoctors {

    private final DoctorQueryGateway doctorQueryGateway;
    private final VisitQueryGateway visitQueryGateway;

    public LoadDoctorsUsecase(DoctorQueryGateway doctorQueryGateway, VisitQueryGateway visitQueryGateway) {
        this.visitQueryGateway = visitQueryGateway;
        this.doctorQueryGateway = doctorQueryGateway;
    }

    @Override
    public List<Doctor> loadAll() {
        return doctorQueryGateway.loadAll();
    }

    @Override
    public Doctor loadByLastName(String lastName) {
        return doctorQueryGateway.loadByLastName(lastName);
    }

    @Override
    public Doctor loadById(EntityId id) {
        return doctorQueryGateway.loadById(id);
    }

    @Override
    public List<Doctor> loadAvailableDoctorsOn(LocalDateTime visitDate) {
        Set<Doctor> unavailableDoctors = loadUnavailableDoctors(visitDate);
        List<Doctor> allDoctors = loadAll();
        allDoctors.removeAll(unavailableDoctors);
        return allDoctors;
    }

    private Set<Doctor> loadUnavailableDoctors(LocalDateTime visitDate) {
        return visitQueryGateway.loadAllByDate(visitDate)
                .stream()
                .map(Visit::getDoctor).collect(Collectors.toSet());
    }
}
