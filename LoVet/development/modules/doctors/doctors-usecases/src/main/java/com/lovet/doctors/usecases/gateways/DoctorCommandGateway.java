package com.lovet.doctors.usecases.gateways;

import com.lovet.EntityId;
import com.lovet.doctors.domain.Doctor;

public interface DoctorCommandGateway {

    void save(Doctor doctor);

    void deleteById(EntityId id);
}
