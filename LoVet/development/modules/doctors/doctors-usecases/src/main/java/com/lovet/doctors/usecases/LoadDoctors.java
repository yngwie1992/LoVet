package com.lovet.doctors.usecases;

import com.lovet.EntityId;
import com.lovet.doctors.domain.Doctor;

import java.time.LocalDateTime;
import java.util.List;

public interface LoadDoctors {

    List<Doctor> loadAll();

    //TODO what when there are 2 doctors with the same lastname?
    Doctor loadByLastName(String lastName);

    Doctor loadById(EntityId id);

    List<Doctor> loadAvailableDoctorsOn(LocalDateTime visitDate);
}
