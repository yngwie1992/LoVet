package com.lovet.doctors.usecases.gateways;

import com.lovet.EntityId;
import com.lovet.doctors.domain.Doctor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface DoctorQueryGateway {

    Doctor loadByLastName(String lastname);

    List<Doctor> loadAll();

    Doctor loadById(EntityId id);
}
