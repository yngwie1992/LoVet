package com.lovet.doctors.usecases;

import com.lovet.doctors.domain.Doctor;

public interface SaveDoctors {

    void save(Doctor doctor);
}
