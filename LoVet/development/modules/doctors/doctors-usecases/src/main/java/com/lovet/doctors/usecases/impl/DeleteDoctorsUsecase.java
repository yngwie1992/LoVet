package com.lovet.doctors.usecases.impl;

import com.lovet.EntityId;
import com.lovet.doctors.usecases.DeleteDoctors;
import com.lovet.doctors.usecases.gateways.DoctorCommandGateway;

public class DeleteDoctorsUsecase implements DeleteDoctors {

    private final DoctorCommandGateway doctorCommandGateway;

    public DeleteDoctorsUsecase(DoctorCommandGateway doctorCommandGateway) {
        this.doctorCommandGateway = doctorCommandGateway;
    }

    @Override
    public void deleteById(EntityId id) {
        doctorCommandGateway.deleteById(id);
    }
}
