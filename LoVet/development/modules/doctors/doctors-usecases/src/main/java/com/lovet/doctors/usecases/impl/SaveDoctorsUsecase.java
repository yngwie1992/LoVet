package com.lovet.doctors.usecases.impl;

import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.usecases.SaveDoctors;
import com.lovet.doctors.usecases.gateways.DoctorCommandGateway;

public class SaveDoctorsUsecase implements SaveDoctors {

    private final DoctorCommandGateway doctorCommandGateway;

    public SaveDoctorsUsecase(DoctorCommandGateway doctorCommandGateway) {
        this.doctorCommandGateway = doctorCommandGateway;
    }

    @Override
    public void save(Doctor doctor) {
        doctorCommandGateway.save(doctor);
    }
}
