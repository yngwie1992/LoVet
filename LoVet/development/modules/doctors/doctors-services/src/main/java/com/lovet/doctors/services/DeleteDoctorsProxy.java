package com.lovet.doctors.services;

import com.lovet.EntityId;
import com.lovet.doctors.usecases.DeleteDoctors;
import com.lovet.doctors.usecases.gateways.DoctorCommandGateway;
import com.lovet.doctors.usecases.impl.DeleteDoctorsUsecase;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DeleteDoctorsProxy implements DeleteDoctors {

    private final DeleteDoctors deleteDoctors;

    public DeleteDoctorsProxy(DoctorCommandGateway doctorCommandGateway) {
        deleteDoctors = new DeleteDoctorsUsecase(doctorCommandGateway);
    }

    @Transactional
    @Override
    public void deleteById(EntityId id) {
        deleteDoctors.deleteById(id);
    }
}
