package com.lovet.doctors.services;

import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.usecases.SaveDoctors;
import com.lovet.doctors.usecases.gateways.DoctorCommandGateway;
import com.lovet.doctors.usecases.impl.SaveDoctorsUsecase;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SaveDoctorsProxy implements SaveDoctors {

    private final SaveDoctors saveDoctors;

    public SaveDoctorsProxy(DoctorCommandGateway doctorCommandGateway) {
        saveDoctors = new SaveDoctorsUsecase(doctorCommandGateway);
    }

    @Transactional
    @Override
    public void save(Doctor doctor) {
        saveDoctors.save(doctor);
    }
}
