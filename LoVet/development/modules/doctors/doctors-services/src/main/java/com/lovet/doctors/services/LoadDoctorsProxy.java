package com.lovet.doctors.services;

import com.lovet.EntityId;
import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.usecases.LoadDoctors;
import com.lovet.doctors.usecases.gateways.DoctorQueryGateway;
import com.lovet.doctors.usecases.impl.LoadDoctorsUsecase;
import com.lovet.visits.usecases.gateways.VisitQueryGateway;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class LoadDoctorsProxy implements LoadDoctors {

    private final LoadDoctors loadDoctors;

    public LoadDoctorsProxy(DoctorQueryGateway doctorQueryGateway, VisitQueryGateway visitQueryGateway) {
        loadDoctors = new LoadDoctorsUsecase(doctorQueryGateway, visitQueryGateway);
    }

    @Override
    public List<Doctor> loadAll() {
        return loadDoctors.loadAll();
    }

    @Override
    public Doctor loadByLastName(String lastName) {
        return loadDoctors.loadByLastName(lastName);
    }

    @Override
    public Doctor loadById(EntityId id) {
        return loadDoctors.loadById(id);
    }

    @Override
    public List<Doctor> loadAvailableDoctorsOn(LocalDateTime date) {
        return loadDoctors.loadAvailableDoctorsOn(date);
    }
}
