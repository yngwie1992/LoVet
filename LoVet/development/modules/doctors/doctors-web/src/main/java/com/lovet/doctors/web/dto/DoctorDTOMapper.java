package com.lovet.doctors.web.dto;

import com.lovet.doctors.domain.Doctor;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;

public class DoctorDTOMapper<T> {

    private final ModelMapper mapper = new ModelMapper();

    public DoctorDTOMapper() {
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
    }

    public Doctor toDoctor(T doctorDTO) {
        return mapper.map(doctorDTO, Doctor.class);
    }

    public LoadDoctorDTO toLoadDoctorDTO(Doctor doctor) {
        return mapper.map(doctor, LoadDoctorDTO.class);
    }
}
