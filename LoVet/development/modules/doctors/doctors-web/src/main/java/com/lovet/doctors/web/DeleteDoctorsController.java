package com.lovet.doctors.web;

import com.lovet.EntityId;
import com.lovet.doctors.usecases.DeleteDoctors;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Doctors")
public class DeleteDoctorsController {

    private final DeleteDoctors deleteDoctors;

    public DeleteDoctorsController(DeleteDoctors deleteDoctors) {
        this.deleteDoctors = deleteDoctors;
    }

    @DeleteMapping("/doctors")
    public void delete(String doctorId) {
        deleteDoctors.deleteById(EntityId.fromString(doctorId));
    }
}
