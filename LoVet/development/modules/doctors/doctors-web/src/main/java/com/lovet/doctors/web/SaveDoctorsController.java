package com.lovet.doctors.web;

import com.lovet.doctors.usecases.SaveDoctors;
import com.lovet.doctors.web.dto.DoctorDTOMapper;
import com.lovet.doctors.web.dto.SaveDoctorDTO;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Doctors")
public class SaveDoctorsController {

    private final SaveDoctors saveDoctors;
    private final DoctorDTOMapper<SaveDoctorDTO> doctorDTOMapper = new DoctorDTOMapper<>();

    public SaveDoctorsController(SaveDoctors saveDoctors) {
        this.saveDoctors = saveDoctors;
    }

    @PostMapping("/doctors")
    public void save(@RequestBody SaveDoctorDTO doctorDTO) {
        saveDoctors.save(doctorDTOMapper.toDoctor(doctorDTO));
    }
}
