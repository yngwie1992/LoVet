package com.lovet.doctors.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;

public class LoadDoctorDTO {

    @JsonProperty("id")
    private String idValue;

    @ApiModelProperty(
            example = "DOCTOR"
    )
    private String role;

    @Size(min = 6, max = 20)
    private String firstName;

    @Size(min = 6, max = 20)
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdValue() {
        return idValue;
    }

    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
