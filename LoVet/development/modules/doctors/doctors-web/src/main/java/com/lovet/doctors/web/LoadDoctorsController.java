package com.lovet.doctors.web;

import com.lovet.doctors.usecases.LoadDoctors;
import com.lovet.doctors.web.dto.DoctorDTOMapper;
import com.lovet.doctors.web.dto.LoadDoctorDTO;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Doctors")
public class LoadDoctorsController {

    private final LoadDoctors loadDoctors;
    private final DoctorDTOMapper<LoadDoctorDTO> doctorDTOMapper = new DoctorDTOMapper<>();

    public LoadDoctorsController(LoadDoctors loadDoctors) {
        this.loadDoctors = loadDoctors;
    }

    @GetMapping("/doctors")
    public List<LoadDoctorDTO> loadAll() {
        return loadDoctors.loadAll()
                .stream()
                .map(doctorDTOMapper::toLoadDoctorDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/doctors/available/{visitDate}")
    public List<LoadDoctorDTO> loadDoctorsAvailableOn(@PathVariable("visitDate") String visitDateString) {
        return loadDoctors.loadAvailableDoctorsOn(parseLocalDateTimeFromString(visitDateString)).stream()
                .map(doctorDTOMapper::toLoadDoctorDTO)
                .collect(Collectors.toList());
    }

    private LocalDateTime parseLocalDateTimeFromString(String localDateTimeString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d HH:mm");
        return LocalDateTime.parse(localDateTimeString, formatter);
    }
}
