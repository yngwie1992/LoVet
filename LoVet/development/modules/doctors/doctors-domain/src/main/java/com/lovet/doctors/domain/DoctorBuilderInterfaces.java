package com.lovet.doctors.domain;

import java.util.Set;

final class DoctorBuilderInterfaces {

    public interface FirstNameInterface {
        LastNameInterface withFirstName(String fistName);
    }

    public interface LastNameInterface {
        CredentialsInterface withLastName(String lastName);
    }

    public interface CredentialsInterface {
        SpecialitiesInterface withCredentials(String email, String password);
    }

    public interface SpecialitiesInterface {
        Doctor andSpecialities(Set<Speciality> specialities);
    }
}
