package com.lovet.doctors.domain;

import java.util.Set;

public class DoctorBuilder implements
                DoctorBuilderInterfaces.FirstNameInterface,
                DoctorBuilderInterfaces.LastNameInterface,
                DoctorBuilderInterfaces.CredentialsInterface,
                DoctorBuilderInterfaces.SpecialitiesInterface {

    private Doctor doctor;

    public DoctorBuilder() {
        doctor = new Doctor();
    }

    @Override
    public DoctorBuilderInterfaces.LastNameInterface withFirstName(String fistName) {
        doctor.setFirstName(fistName);
        return this;
    }

    @Override
    public DoctorBuilderInterfaces.CredentialsInterface withLastName(String lastName) {
        doctor.setLastName(lastName);
        return this;
    }

    @Override
    public DoctorBuilderInterfaces.SpecialitiesInterface withCredentials(String email, String password) {
        doctor.setCredentials(new Credentials(email, password));
        return this;
    }

    @Override
    public Doctor andSpecialities(Set<Speciality> specialities) {
        doctor.addSpecialities(specialities);
        return doctor;
    }
}
