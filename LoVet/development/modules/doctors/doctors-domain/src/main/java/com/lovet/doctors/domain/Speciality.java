package com.lovet.doctors.domain;

public enum Speciality {
    CATS, DOGS, RABBITS, SNAKES
}
