package com.lovet.doctors.domain;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Doctor {

    private Credentials credentials;
    private String firstName;
    private String lastName;
    private Set<Speciality> specialities = new HashSet<>();

    Doctor() {
    }

    public static DoctorBuilderInterfaces.FirstNameInterface create() {
        return new DoctorBuilder();
    }

    public String getFirstName() {
        return firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public Set<Speciality> getSpecialities() {
        return Collections.unmodifiableSet(specialities);
    }

    void addSpecialities(Set<Speciality> specialities) {
        this.specialities.addAll(specialities);
    }
}
