package com.lovet.doctors.gateways.repositories;

import com.lovet.EntityId;
import com.lovet.doctors.gateways.entities.DoctorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorsRepository extends JpaRepository<DoctorEntity, EntityId> {
    DoctorEntity findByLastName(String lastName);
}
