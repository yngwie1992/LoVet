package com.lovet.doctors.gateways;

import com.lovet.EntityId;
import com.lovet.Role;
import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.gateways.entities.DoctorEntity;
import com.lovet.doctors.gateways.entities.mappers.DoctorGatewayMapper;
import com.lovet.doctors.gateways.repositories.DoctorsRepository;
import com.lovet.doctors.usecases.gateways.DoctorCommandGateway;
import com.lovet.doctors.usecases.gateways.DoctorQueryGateway;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DatabaseDoctorsGateway implements DoctorQueryGateway, DoctorCommandGateway {

    private final DoctorsRepository doctorsRepository;
    private final DoctorGatewayMapper mapper;

    public DatabaseDoctorsGateway(DoctorsRepository doctorsRepository, DoctorGatewayMapper mapper) {
        this.doctorsRepository = doctorsRepository;
        this.mapper = mapper;
    }

    @Override
    public Doctor loadByLastName(String lastName) {
        DoctorEntity doctorEntity = doctorsRepository.findByLastName(lastName);
        return mapper.mapToDoctor(doctorEntity);
    }

    @Override
    public List<Doctor> loadAll() {
        List<DoctorEntity> doctorEntities = doctorsRepository.findAll();
        return doctorEntities.stream().map(mapper::mapToDoctor).collect(Collectors.toList());
    }

    @Override
    public Doctor loadById(EntityId id) {
        DoctorEntity doctorEntity = doctorsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Doctor id [%s] not found",
                        id.getValue())));
        return mapper.mapToDoctor(doctorEntity);
    }

    @Override
    public void save(Doctor doctor) {
        DoctorEntity doctorEntity = mapper.mapToDoctorEntity(doctor);
        doctorEntity.setRole(Role.DOCTOR);
        doctorsRepository.save(doctorEntity);
    }

    @Override
    public void deleteById(EntityId id) {
        doctorsRepository.deleteById(id);
    }
}
