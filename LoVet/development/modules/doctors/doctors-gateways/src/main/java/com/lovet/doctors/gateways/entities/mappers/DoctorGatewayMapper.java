package com.lovet.doctors.gateways.entities.mappers;

import com.lovet.EntityId;
import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.gateways.entities.DoctorEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class DoctorGatewayMapper {

    private final ModelMapper mapper = new ModelMapper();

    public DoctorGatewayMapper() {
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
    }

    public Doctor mapToDoctor(DoctorEntity doctorEntity) {
        return mapper.map(doctorEntity, Doctor.class);
    }

    public DoctorEntity mapToDoctorEntity(Doctor doctor) {
        //todo short term, It has to change in order to update doctors
        DoctorEntity doctorEntity = mapper.map(doctor, DoctorEntity.class);
        doctorEntity.setId(EntityId.create());
        return doctorEntity;
    }
}
