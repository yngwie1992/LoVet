package com.lovet.doctors.gateways.entities;

import com.lovet.User;
import com.lovet.doctors.domain.Speciality;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class DoctorEntity extends User {

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="DOCTOR_SPECIALITIES",
            joinColumns = @JoinColumn(name = "DOCTOR_ID"))
    @Column(name = "SPECIALITY")
    private Set<Speciality> specialities = new HashSet<>();

    private DoctorEntity() {
    }

    public Set<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(Set<Speciality> specialities) {
        this.specialities = specialities;
    }
}
