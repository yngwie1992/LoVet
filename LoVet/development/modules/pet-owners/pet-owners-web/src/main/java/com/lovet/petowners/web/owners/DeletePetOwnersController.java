package com.lovet.petowners.web.owners;


import com.lovet.petowners.usecases.owners.DeletePetOwners;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Pet Owners")
public class DeletePetOwnersController {

    private final DeletePetOwners deletePetOwners;

    public DeletePetOwnersController(DeletePetOwners deletePetOwners) {
        this.deletePetOwners = deletePetOwners;
    }

    @DeleteMapping("/owners/username/{username}")
    public void deleteByUsername(@PathVariable String username) {
        deletePetOwners.deleteByEmail(username);
    }
}
