package com.lovet.petowners.web.owners.dto;


import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.web.pets.dto.LoadPetMapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PetOwnerMapper<T> {

    private final ModelMapper mapper = new ModelMapper();
    private final PasswordEncoder passwordEncoder;

    public PetOwnerMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
    }

    public PetOwner mapToEntity(T petOwnerDTO) {
        PetOwner petOwner = mapper.map(petOwnerDTO, PetOwner.class);
        encodePassword(petOwner);
        return petOwner;
    }

    private void encodePassword(PetOwner petOwner) {
        petOwner.setPassword(passwordEncoder.encode(petOwner.getPassword()));
    }

    public LoadPetOwnerDTO mapToLoadDTO(PetOwner petOwner) {
        return mapper.map(petOwner, LoadPetOwnerDTO.class);
    }
}
