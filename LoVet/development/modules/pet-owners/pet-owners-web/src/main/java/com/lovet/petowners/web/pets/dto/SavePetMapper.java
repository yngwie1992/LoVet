package com.lovet.petowners.web.pets.dto;

import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.usecases.owners.LoadPetOwners;
import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.domain.pets.PetType;
import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class SavePetMapper {
    private final ModelMapper mapper = new ModelMapper();
    private final LoadPetOwners loadPetOwners;

    public SavePetMapper(LoadPetOwners loadPetOwners) {
        this.loadPetOwners = loadPetOwners;
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        mapper.addConverter(enumToString());
        //mapper.addConverter(emailToPetOwner());
        mapper.addConverter(stringDateToDate());
    }

    public Pet mapToPet(SavePetDTO petDTO) {
        return mapper.map(petDTO, Pet.class);
    }

    public SavePetDTO mapToDTO(Pet pet) {
        return mapper.map(pet, SavePetDTO.class);
    }

    private AbstractConverter<PetType, String> enumToString() {
        return new AbstractConverter<PetType, String>() {
            protected String convert(PetType petType) {
                return petType.toString();
            }
        };
    }

/*    private AbstractConverter<String, PetOwner> emailToPetOwner() {
        return new AbstractConverter<String, PetOwner>() {
            protected PetOwner convert(String petOwnerEmail) {
                return loadPetOwners.loadByEmail(petOwnerEmail);
            }
        };
    }*/

    private AbstractConverter<String, LocalDate> stringDateToDate() {
        return new AbstractConverter<String, LocalDate>() {
            protected LocalDate convert(String stringDate) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
                return LocalDate.parse(stringDate, formatter);
            }
        };
    }
}
