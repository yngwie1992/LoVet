package com.lovet.petowners.web.pets.dto;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.domain.pets.PetType;
import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class LoadPetMapper {

    private ModelMapper mapper;

    public LoadPetMapper() {
        mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);

        mapper.addConverter(convertLocalDateToString());
        mapper.addConverter(convertPetTypeToString());
    }

    public Pet toPet(LoadPetDTO petDTO) {
        return mapper.map(petDTO, Pet.class);
    }

    public LoadPetDTO toPetDTO(Pet pet) {
        return mapper.map(pet, LoadPetDTO.class);
    }

    private AbstractConverter<LocalDate, String> convertLocalDateToString() {
        return new AbstractConverter<LocalDate, String>() {
            protected String convert(LocalDate date) {
                return date.toString();
            }
        };
    }

    private AbstractConverter<PetType, String> convertPetTypeToString() {
        return new AbstractConverter<PetType, String>() {
            protected String convert(PetType petType) {
                return petType.toString();
            }
        };
    }
}
