package com.lovet.petowners.web.owners.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoadPetOwnerDTO {

    private String firstName;
    private String lastName;
    private long phoneNumber;
    @JsonProperty(value = "email")
    private String credentialsEmail;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCredentialsEmail() {
        return credentialsEmail;
    }

    public void setCredentialsEmail(String credentialsEmail) {
        this.credentialsEmail = credentialsEmail;
    }
}
