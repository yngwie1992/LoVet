package com.lovet.petowners.web.owners;

import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.usecases.owners.LoadPetOwners;
import com.lovet.petowners.web.owners.dto.LoadPetOwnerDTO;
import com.lovet.petowners.web.owners.dto.PetOwnerMapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Pet Owners")
public class LoadPetOwnersController {

    private final LoadPetOwners loadPetOwners;
    private final PetOwnerMapper<LoadPetOwnerDTO> mapper;

    public LoadPetOwnersController(LoadPetOwners loadPetOwners, PetOwnerMapper<LoadPetOwnerDTO> mapper) {
        this.loadPetOwners = loadPetOwners;
        this.mapper = mapper;
    }

    @GetMapping("/owners")
    public List<LoadPetOwnerDTO> loadAll() {
        return loadPetOwners.loadAll()
                .stream()
                .map(mapper::mapToLoadDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/owners/username/{username}")
    public LoadPetOwnerDTO loadByUsername(@PathVariable String username) {
        PetOwner petOwner = loadPetOwners.loadByEmail(username);
        return mapper.mapToLoadDTO(petOwner);
    }
}
