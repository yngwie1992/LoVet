package com.lovet.petowners.web.owners;

import com.lovet.petowners.usecases.owners.SavePetOwners;
import com.lovet.petowners.web.owners.dto.PetOwnerMapper;
import com.lovet.petowners.web.owners.dto.SavePetOwnerDTO;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Api(tags = "Pet Owners")
public class RegisterPetOwnersController {

    private final SavePetOwners savePetOwners;
    private final PetOwnerMapper<SavePetOwnerDTO> petOwnerMapper;

    public RegisterPetOwnersController(SavePetOwners savePetOwners, PetOwnerMapper<SavePetOwnerDTO> petOwnerMapper) {
        this.savePetOwners = savePetOwners;
        this.petOwnerMapper = petOwnerMapper;
    }

    @PostMapping("/owners/register")
    public void save(@RequestBody @Valid SavePetOwnerDTO savePetOwnerDTO) {
        verifyPasswordsEquality(savePetOwnerDTO.getCredentialsPassword(), savePetOwnerDTO.getRepeatPassword());
        savePetOwners.save(petOwnerMapper.mapToEntity(savePetOwnerDTO));
    }

    private void verifyPasswordsEquality(String password, String repeatPassword) {
        if(!password.equals(repeatPassword)) {
            throw new PasswordsDoesNotMatch();
        }
    }
}
