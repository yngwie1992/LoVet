package com.lovet.petowners.web.pets.dto;

public class SavePetDTO {

    private String name;
    private String petType;
    private String dateOfBirth;
    private Integer age;
    private String petOwnerEmail;

    public String getPetOwnerEmail() {
        return petOwnerEmail;
    }

    public void setPetOwnerEmail(String petOwnerEmail) {
        this.petOwnerEmail = petOwnerEmail;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getPetType() {
        return petType;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
