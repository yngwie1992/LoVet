package com.lovet.petowners.web.pets;

import com.lovet.petowners.services.pets.SavePetsProxy;
import com.lovet.petowners.web.pets.dto.SavePetDTO;
import com.lovet.petowners.web.pets.dto.SavePetMapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@Api(tags = "Pets")
public class SavePetsController {

    private final SavePetMapper mapper;
    private final SavePetsProxy savePets;

    public SavePetsController(SavePetMapper mapper, SavePetsProxy savePets) {
        this.mapper = mapper;
        this.savePets = savePets;
    }

    @PostMapping("/pets")
    public void save(@RequestBody SavePetDTO petDTO, Principal principal) {
        petDTO.setPetOwnerEmail(principal.getName());
        savePets.save(mapper.mapToPet(petDTO));
    }
}
