package com.lovet.petowners.web.pets;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.LoadPets;
import com.lovet.petowners.web.pets.dto.LoadPetDTO;
import com.lovet.petowners.web.pets.dto.LoadPetMapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Pets")
public class LoadPetsController {

    private final LoadPetMapper loadPetMapper;

    public LoadPetsController(LoadPetMapper loadPetMapper, LoadPets loadPets) {
        this.loadPetMapper = loadPetMapper;
        this.loadPets = loadPets;
    }

    private final LoadPets loadPets;

    @GetMapping("/pets")
    public List<LoadPetDTO> loadAll() {
        List<Pet> pets = loadPets.loadAll();
        return pets.stream()
                .map(loadPetMapper::toPetDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/pets/{email}")
    public List<LoadPetDTO> loadByPetOwnerEmail(@PathVariable String email) {
        return loadPets.loadByPetOwnerEmail(email)
                .stream()
                .map(loadPetMapper::toPetDTO)
                .collect(Collectors.toList());
    }
}
