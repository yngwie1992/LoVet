package com.lovet.petowners.web.owners;

public class PasswordsDoesNotMatch extends RuntimeException {

    public PasswordsDoesNotMatch() {
        super("Given password and repeatPassword does not match");
    }
}
