package com.lovet.petowners.usecases.owners.gateways;

import java.util.List;

import com.lovet.petowners.domain.owners.PetOwner;

public interface PetOwnersQueryGateway
{
    List<PetOwner> loadAll();

    PetOwner loadByEmail(String email);

    boolean existsByEmail(String email);
}
