package com.lovet.petowners.usecases.pets.gateways;

import com.lovet.petowners.domain.pets.Pet;

import java.util.List;

public interface PetCommandGateway {

    void save(Pet pet);

    void saveAll(List<Pet> pets);

    void deleteById(Long id);

    void delete(Pet pet);
}
