package com.lovet.petowners.usecases.owners;

import com.lovet.petowners.domain.owners.PetOwner;

import java.util.List;

public interface LoadPetOwners {

    List<PetOwner> loadAll();

    PetOwner loadByEmail(String email);

    boolean existsByEmail(String email);
}
