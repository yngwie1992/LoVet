package com.lovet.petowners.usecases.pets.impl;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.LoadPets;
import com.lovet.petowners.usecases.pets.gateways.PetQueryGateway;

import java.util.List;

public class LoadPetsUsecase implements LoadPets {

    private final PetQueryGateway petQueryGateway;

    public LoadPetsUsecase(PetQueryGateway petQueryGateway) {
        this.petQueryGateway = petQueryGateway;
    }

    @Override
    public List<Pet> loadAll() {
        return petQueryGateway.loadAll();
    }

    @Override
    public List<Pet> loadByPetOwnerEmail(String email) {
        return petQueryGateway.loadByPetOwnerEmail(email);
    }

    @Override
    public Pet loadById(Long petId) {
        return petQueryGateway.loadById(petId);
    }
}
