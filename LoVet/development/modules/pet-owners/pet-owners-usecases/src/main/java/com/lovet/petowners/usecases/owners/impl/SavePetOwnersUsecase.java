package com.lovet.petowners.usecases.owners.impl;

import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.usecases.owners.LoadPetOwners;
import com.lovet.petowners.usecases.owners.SavePetOwners;
import com.lovet.petowners.usecases.owners.gateways.PetOwnerCommandGateway;

public class SavePetOwnersUsecase implements SavePetOwners {

    private final PetOwnerCommandGateway petOwnerCommandGateway;
    private final LoadPetOwners loadPetOwners;

    public SavePetOwnersUsecase(PetOwnerCommandGateway petOwnerCommandGateway, LoadPetOwners loadPetOwners) {
        this.petOwnerCommandGateway = petOwnerCommandGateway;
        this.loadPetOwners = loadPetOwners;
    }

    @Override
    public void save(PetOwner petOwner) {
        verifyUniqueUsername(petOwner.getEmail());
        petOwnerCommandGateway.save(petOwner);
    }

    private void verifyUniqueUsername(String email) {
        loadPetOwners.existsByEmail(email);
    }
}
