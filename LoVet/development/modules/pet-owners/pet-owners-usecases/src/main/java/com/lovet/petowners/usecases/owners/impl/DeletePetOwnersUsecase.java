package com.lovet.petowners.usecases.owners.impl;

import com.lovet.petowners.usecases.owners.DeletePetOwners;
import com.lovet.petowners.usecases.owners.gateways.PetOwnerCommandGateway;

public class DeletePetOwnersUsecase implements DeletePetOwners {

    private final PetOwnerCommandGateway petOwnerCommandGateway;

    public DeletePetOwnersUsecase(PetOwnerCommandGateway petOwnerCommandGateway) {
        this.petOwnerCommandGateway = petOwnerCommandGateway;
    }

    @Override
    public void deleteByEmail(String username) {
        petOwnerCommandGateway.deleteByUsername(username);
    }
}
