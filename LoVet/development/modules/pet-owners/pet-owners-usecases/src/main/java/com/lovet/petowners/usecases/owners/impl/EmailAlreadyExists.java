package com.lovet.petowners.usecases.owners.impl;

public class EmailAlreadyExists extends RuntimeException {

    public EmailAlreadyExists(String email) {
        super(String.format("Email [%s] already exist in database", email));
    }
}
