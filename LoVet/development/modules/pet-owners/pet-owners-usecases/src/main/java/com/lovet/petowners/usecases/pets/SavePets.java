package com.lovet.petowners.usecases.pets;

import com.lovet.petowners.domain.pets.Pet;

import java.util.List;

public interface SavePets {

    void save(Pet pet);

    void saveAll(List<Pet> pets);
}
