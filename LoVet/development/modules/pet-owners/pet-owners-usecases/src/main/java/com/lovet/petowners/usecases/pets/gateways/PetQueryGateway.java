package com.lovet.petowners.usecases.pets.gateways;

import com.lovet.petowners.domain.pets.Pet;

import java.util.List;

public interface PetQueryGateway {

    List<Pet> loadByPetOwnerEmail(String email);

    Pet loadById(Long petId);

    List<Pet> loadAll();
}
