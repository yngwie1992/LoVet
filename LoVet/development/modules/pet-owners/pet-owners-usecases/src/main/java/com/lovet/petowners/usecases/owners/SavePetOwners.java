package com.lovet.petowners.usecases.owners;

import com.lovet.petowners.domain.owners.PetOwner;

public interface SavePetOwners {

    void save(PetOwner petOwner);
}
