package com.lovet.petowners.usecases.owners;

public interface DeletePetOwners {

    void deleteByEmail(String email);
}
