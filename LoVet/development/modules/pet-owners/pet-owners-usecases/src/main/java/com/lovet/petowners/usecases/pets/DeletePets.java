package com.lovet.petowners.usecases.pets;

import com.lovet.petowners.domain.pets.Pet;

public interface DeletePets {

    void deleteById(Long id);

    void delete(Pet pet);
}
