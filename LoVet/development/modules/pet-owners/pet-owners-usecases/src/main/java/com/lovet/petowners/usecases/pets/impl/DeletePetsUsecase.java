package com.lovet.petowners.usecases.pets.impl;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.DeletePets;
import com.lovet.petowners.usecases.pets.gateways.PetCommandGateway;

public class DeletePetsUsecase implements DeletePets {

    private final PetCommandGateway petCommandGateway;

    public DeletePetsUsecase(PetCommandGateway petCommandGateway) {
        this.petCommandGateway = petCommandGateway;
    }

    @Override
    public void deleteById(Long id) {
        petCommandGateway.deleteById(id);
    }

    @Override
    public void delete(Pet pet) {
        petCommandGateway.delete(pet);
    }
}
