package com.lovet.petowners.usecases.pets.impl;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.SavePets;
import com.lovet.petowners.usecases.pets.gateways.PetCommandGateway;

import java.util.List;

public class SavePetUsecase implements SavePets {

    private final PetCommandGateway petCommandGateway;

    public SavePetUsecase(PetCommandGateway petCommandGateway) {
        this.petCommandGateway = petCommandGateway;
    }

    public void save(Pet pet) {
        petCommandGateway.save(pet);
    }

    @Override
    public void saveAll(List<Pet> pets) {
        petCommandGateway.saveAll(pets);
    }
}
