package com.lovet.petowners.usecases.pets;

import com.lovet.petowners.domain.pets.Pet;

import java.util.List;

public interface LoadPets {

    List<Pet> loadAll();

    List<Pet> loadByPetOwnerEmail(String email);

    Pet loadById(Long petId);
}
