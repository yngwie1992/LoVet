package com.lovet.petowners.usecases.owners.gateways;

import com.lovet.petowners.domain.owners.PetOwner;

public interface PetOwnerCommandGateway {

    void save(PetOwner owner);

    void deleteByUsername(String username);
}
