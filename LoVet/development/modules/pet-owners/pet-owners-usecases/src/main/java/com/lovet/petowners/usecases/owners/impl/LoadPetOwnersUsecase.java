package com.lovet.petowners.usecases.owners.impl;

import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.usecases.owners.LoadPetOwners;
import com.lovet.petowners.usecases.owners.gateways.PetOwnersQueryGateway;

import java.util.List;

public class LoadPetOwnersUsecase implements LoadPetOwners {

    private final PetOwnersQueryGateway petOwnersQueryGateway;

    public LoadPetOwnersUsecase(PetOwnersQueryGateway petOwnersQueryGateway) {
        this.petOwnersQueryGateway = petOwnersQueryGateway;
    }

    @Override
    public List<PetOwner> loadAll() {
        return petOwnersQueryGateway.loadAll();
    }

    @Override
    public PetOwner loadByEmail(String username) {
        return petOwnersQueryGateway.loadByEmail(username);
    }

    @Override
    public boolean existsByEmail(String username) {
        return petOwnersQueryGateway.existsByEmail(username);
    }
}
