package com.lovet.petowners.services.owners;

import com.lovet.petowners.usecases.owners.DeletePetOwners;
import com.lovet.petowners.usecases.owners.gateways.PetOwnerCommandGateway;
import com.lovet.petowners.usecases.owners.impl.DeletePetOwnersUsecase;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DeletePetOwnersProxy implements DeletePetOwners {

    private final DeletePetOwners deletePetOwners;

    public DeletePetOwnersProxy(PetOwnerCommandGateway petOwnerCommandGateway) {
        this.deletePetOwners = new DeletePetOwnersUsecase(petOwnerCommandGateway);
    }


    @Transactional
    @Override
    public void deleteByEmail(String email) {
        deletePetOwners.deleteByEmail(email);
    }
}
