package com.lovet.petowners.services.pets;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.SavePets;
import com.lovet.petowners.usecases.pets.gateways.PetCommandGateway;
import com.lovet.petowners.usecases.pets.impl.SavePetUsecase;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class SavePetsProxy implements SavePets {

    private final SavePetUsecase savePets;

    public SavePetsProxy(PetCommandGateway petCommandGateway) {
        this.savePets = new SavePetUsecase(petCommandGateway);
    }

    @Transactional
    @Override
    public void save(Pet pet) {
        savePets.save(pet);
    }

    @Override
    public void saveAll(List<Pet> pets) {
        savePets.saveAll(pets);
    }
}
