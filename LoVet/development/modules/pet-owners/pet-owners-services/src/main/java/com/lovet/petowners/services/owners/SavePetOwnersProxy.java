package com.lovet.petowners.services.owners;

import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.usecases.owners.LoadPetOwners;
import com.lovet.petowners.usecases.owners.SavePetOwners;
import com.lovet.petowners.usecases.owners.gateways.PetOwnerCommandGateway;
import com.lovet.petowners.usecases.owners.impl.SavePetOwnersUsecase;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SavePetOwnersProxy implements SavePetOwners {

    private final SavePetOwners savePetOwners;

    public SavePetOwnersProxy(PetOwnerCommandGateway petOwnerCommandGateway, LoadPetOwners loadPetOwners) {
        savePetOwners = new SavePetOwnersUsecase(petOwnerCommandGateway, loadPetOwners);
    }

    @Transactional
    @Override
    public void save(PetOwner petOwner) {
        savePetOwners.save(petOwner);
    }
}
