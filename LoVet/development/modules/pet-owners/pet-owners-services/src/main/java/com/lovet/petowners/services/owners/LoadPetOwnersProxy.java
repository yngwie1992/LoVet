package com.lovet.petowners.services.owners;

import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.usecases.owners.LoadPetOwners;
import com.lovet.petowners.usecases.owners.gateways.PetOwnersQueryGateway;
import com.lovet.petowners.usecases.owners.impl.LoadPetOwnersUsecase;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoadPetOwnersProxy implements LoadPetOwners {

    private final LoadPetOwners loadPetOwners;

    public LoadPetOwnersProxy(PetOwnersQueryGateway petOwnersQueryGateway) {
        this.loadPetOwners = new LoadPetOwnersUsecase(petOwnersQueryGateway);
    }

    @Override
    public List<PetOwner> loadAll() {
        return loadPetOwners.loadAll();
    }

    @Override
    public PetOwner loadByEmail(String email) {
        return loadPetOwners.loadByEmail(email);
    }

    @Override
    public boolean existsByEmail(String email) {
        return loadPetOwners.existsByEmail(email);
    }
}
