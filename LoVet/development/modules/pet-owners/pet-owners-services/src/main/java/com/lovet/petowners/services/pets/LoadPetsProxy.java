package com.lovet.petowners.services.pets;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.LoadPets;
import com.lovet.petowners.usecases.pets.gateways.PetQueryGateway;
import com.lovet.petowners.usecases.pets.impl.LoadPetsUsecase;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoadPetsProxy implements LoadPets {

    private final LoadPets loadPets;

    public LoadPetsProxy(PetQueryGateway petQueryGateway) {
        this.loadPets = new LoadPetsUsecase(petQueryGateway);
    }

    @Override
    public List<Pet> loadAll() {
        return loadPets.loadAll();
    }

    @Override
    public List<Pet> loadByPetOwnerEmail(String email) {
        return loadPets.loadByPetOwnerEmail(email);
    }

    @Override
    public Pet loadById(Long petId) {
        return loadPets.loadById(petId);
    }
}
