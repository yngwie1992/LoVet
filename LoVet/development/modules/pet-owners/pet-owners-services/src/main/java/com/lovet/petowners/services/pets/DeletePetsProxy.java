package com.lovet.petowners.services.pets;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.DeletePets;
import com.lovet.petowners.usecases.pets.gateways.PetCommandGateway;
import com.lovet.petowners.usecases.pets.impl.DeletePetsUsecase;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DeletePetsProxy implements DeletePets {

    private final DeletePets deletePets;

    public DeletePetsProxy(PetCommandGateway petCommandGateway) {
        this.deletePets = new DeletePetsUsecase(petCommandGateway);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        deletePets.deleteById(id);
    }

    @Transactional
    @Override
    public void delete(Pet pet) {
        deletePets.delete(pet);
    }
}
