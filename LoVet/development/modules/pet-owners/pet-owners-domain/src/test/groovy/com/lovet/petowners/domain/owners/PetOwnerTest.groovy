package com.lovet.petowners.domain.owners


import spock.lang.Specification

class PetOwnerTest extends Specification {

    def "should create pet owner with given parameters"()
    {
        given:
        def petOwner = PetOwner.create()
                                .withCredentials("pio@o2.pl","bar")
                                .withFirstName("Piotr")
                                .withLastName("Kosinski")
                                .andPhoneNumber(507732218)
        expect:
        with(petOwner) {
            getEmail() == "pio@o2.pl"
            getPassword() == "bar"
            getFirstName() == "Piotr"
            getLastName() == "Kosinski"
            getPhoneNumber() == 507732218
        }

    }
}
