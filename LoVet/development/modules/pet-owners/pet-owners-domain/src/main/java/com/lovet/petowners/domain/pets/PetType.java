package com.lovet.petowners.domain.pets;

public enum PetType {
    DOG, CAT, BIRD, HAMSTER, RABBIT
}
