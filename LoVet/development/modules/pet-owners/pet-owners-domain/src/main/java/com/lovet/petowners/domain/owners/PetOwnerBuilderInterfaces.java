package com.lovet.petowners.domain.owners;

final class PetOwnerBuilderInterfaces {

    public interface CredentialsInterface {
        FirstNameInterface withCredentials(String email, String password);
    }

    public interface FirstNameInterface {

        LastNameInterface withFirstName(String firstName);
    }

    public interface LastNameInterface {
        PhoneNumberInterface withLastName(String lastName);
    }

    public interface PhoneNumberInterface {
        PetOwner andPhoneNumber(long phoneNumber);
    }
}
