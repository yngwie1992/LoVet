package com.lovet.petowners.domain.owners;

public class PetOwner
{
    private Credentials credentials;
    private String firstName;
    private String lastName;
    private long phoneNumber;

    PetOwner()
    {
    }

    public static PetOwnerBuilderInterfaces.CredentialsInterface create() {
        return new PetOwnerBuilder();
    }

    public String getFirstName() {
        return firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public String getPassword() {
        return credentials.getPassword();
    }

    public String getEmail() {
        return credentials.getEmail();
    }

    public void setPassword(String password) {
        credentials.setPassword(password);
    }

    void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }
}
