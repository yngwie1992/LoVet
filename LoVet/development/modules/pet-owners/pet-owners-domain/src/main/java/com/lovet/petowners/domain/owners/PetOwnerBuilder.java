package com.lovet.petowners.domain.owners;

public class PetOwnerBuilder implements
        PetOwnerBuilderInterfaces.CredentialsInterface,
        PetOwnerBuilderInterfaces.FirstNameInterface,
        PetOwnerBuilderInterfaces.LastNameInterface,
        PetOwnerBuilderInterfaces.PhoneNumberInterface {

    private PetOwner petOwner;

    PetOwnerBuilder() {
        petOwner = new PetOwner();
    }

    @Override
    public PetOwnerBuilderInterfaces.FirstNameInterface withCredentials(String email, String password) {
        petOwner.setCredentials(new Credentials(email, password));
        return this;
    }

    @Override
    public PetOwnerBuilderInterfaces.LastNameInterface withFirstName(String firstName) {
        petOwner.setFirstName(firstName);
        return this;
    }

    @Override
    public PetOwnerBuilderInterfaces.PhoneNumberInterface withLastName(String lastName) {
        petOwner.setLastName(lastName);
        return this;
    }

    @Override
    public PetOwner andPhoneNumber(long phoneNumber) {
        petOwner.setPhoneNumber(phoneNumber);
        return petOwner;
    }
}
