package com.lovet.petowners.domain.pets;

import java.time.LocalDate;

public class Pet
{
    private Long id;

    private String name;

    private PetType petType;

    private LocalDate dateOfBirth;

    private String petOwnerEmail;

    private Pet()
    {
    }

    //TODO make it prittier
    public Pet(String name, PetType petType, LocalDate dateOfBirth, String petOwnerEmail)
    {
        this.petOwnerEmail = petOwnerEmail;
        this.name = name;
        this.petType = petType;
        this.dateOfBirth = dateOfBirth;
    }

    public Long getId()
    {
        return id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public PetType getPetType()
    {
        return petType;
    }

    public void setPetType(PetType petType)
    {
        this.petType = petType;
    }

    public LocalDate getDateOfBirth()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    public String getName()
    {
        return name;
    }

    public String getPetOwnerEmail() {
        return petOwnerEmail;
    }

    public void setPetOwnerEmail(String petOwnerEmail) {
        this.petOwnerEmail = petOwnerEmail;
    }
}
