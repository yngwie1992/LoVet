package com.lovet.petowners.gateways.entities;

import com.lovet.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PET_OWNERS")
public class PetOwnerEntity extends User
{
    @Column(name = "PHONE_NUMBER")
    private long phoneNumber;

    private PetOwnerEntity() {
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
