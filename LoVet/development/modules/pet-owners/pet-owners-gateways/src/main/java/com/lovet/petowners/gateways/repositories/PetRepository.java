package com.lovet.petowners.gateways.repositories;

import java.util.List;

import com.lovet.petowners.gateways.entities.PetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import com.lovet.petowners.domain.pets.Pet;

public interface PetRepository extends JpaRepository<PetEntity, Long> {
    List<PetEntity> findByPetOwnerCredentialsEmail(String email);
}
