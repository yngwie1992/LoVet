package com.lovet.petowners.gateways.repositories;

import com.lovet.EntityId;
import com.lovet.petowners.gateways.entities.PetOwnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PetOwnersRepository extends JpaRepository<PetOwnerEntity, EntityId> {
    Optional<PetOwnerEntity> findByCredentialsEmail(String email);

    boolean existsByCredentialsEmail(String email);

    void deleteByCredentialsEmail(String email);
}
