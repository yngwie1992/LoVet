package com.lovet.petowners.gateways.entities.mappers;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.gateways.entities.PetEntity;
import com.lovet.petowners.gateways.entities.PetOwnerEntity;
import com.lovet.petowners.gateways.repositories.PetOwnersRepository;
import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class PetGatewayMapper {

    private ModelMapper mapper;
    private final PetOwnersRepository petOwnersRepository;

    public PetGatewayMapper(PetOwnersRepository petOwnersRepository) {
        this.petOwnersRepository = petOwnersRepository;
        mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        mapper.addConverter(convertEmailToPetOwnerEntity());
    }

    public PetEntity toPetEntity(Pet pet) {
        return mapper.map(pet, PetEntity.class);
    }

    public Pet toPet(PetEntity petEntity) {
        return mapper.map(petEntity, Pet.class);
    }

    private AbstractConverter<String, PetOwnerEntity> convertEmailToPetOwnerEntity() {
        return new AbstractConverter<String, PetOwnerEntity>() {
            protected PetOwnerEntity convert(String petOwnerEmail) {
                return petOwnersRepository.findByCredentialsEmail(petOwnerEmail)
                        .orElseThrow(() -> new EntityNotFoundException(String.format("Pet owner with email [%s] not found",
                                petOwnerEmail)));
            }
        };
    }
}
