package com.lovet.petowners.gateways;

import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.gateways.entities.PetEntity;
import com.lovet.petowners.gateways.entities.mappers.PetGatewayMapper;
import com.lovet.petowners.gateways.repositories.PetRepository;
import com.lovet.petowners.usecases.pets.gateways.PetQueryGateway;
import com.lovet.petowners.usecases.pets.gateways.PetCommandGateway;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DatabasePetGateway implements PetQueryGateway, PetCommandGateway
{
    private final PetRepository petRepository;
    private final PetGatewayMapper petGatewayMapper;

    public DatabasePetGateway(PetGatewayMapper petGatewayMapper, PetRepository petRepository) {
        this.petGatewayMapper = petGatewayMapper;
        this.petRepository = petRepository;
    }

    @Override
    public List<Pet> loadByPetOwnerEmail(String email) {
        List<PetEntity> petEntities = petRepository.findByPetOwnerCredentialsEmail(email);
        return petEntities.stream().map(petGatewayMapper::toPet).collect(Collectors.toList());
    }

    @Override
    public Pet loadById(Long petId) {
        PetEntity petEntity = petRepository.findById(petId).orElseThrow(() -> new EntityNotFoundException(String.format("Pet with " +
                "id [%s] doesn't exist", petId)));
        return petGatewayMapper.toPet(petEntity);
    }

    @Override
    public List<Pet> loadAll() {
        List<PetEntity> petEntities = petRepository.findAll();
        return petEntities.stream().map(petGatewayMapper::toPet).collect(Collectors.toList());
    }

    @Override
    public void save(Pet pet) {
        petRepository.save(petGatewayMapper.toPetEntity(pet));
    }

    @Override
    public void saveAll(List<Pet> pets) {
        List<PetEntity> petEntities = pets.stream().map(petGatewayMapper::toPetEntity).collect(Collectors.toList());
        petRepository.saveAll(petEntities);
    }

    @Override
    public void deleteById(Long id) {
        petRepository.deleteById(id);
    }

    @Override
    public void delete(Pet pet) {
        petRepository.delete(petGatewayMapper.toPetEntity(pet));
    }
}
