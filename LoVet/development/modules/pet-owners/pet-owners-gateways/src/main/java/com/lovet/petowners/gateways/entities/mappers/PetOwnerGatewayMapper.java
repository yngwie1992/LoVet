package com.lovet.petowners.gateways.entities.mappers;

import com.lovet.EntityId;
import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.gateways.entities.PetOwnerEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.stereotype.Component;

@Component
public class PetOwnerGatewayMapper {

    private ModelMapper mapper = new ModelMapper();

    public PetOwnerGatewayMapper() {
        mapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
    }

    public PetOwnerEntity toPetOwnerEntity(PetOwner petOwner) {
        PetOwnerEntity petOwnerEntity = mapper.map(petOwner, PetOwnerEntity.class);
        petOwnerEntity.setId(EntityId.create());
        return petOwnerEntity;
    }

    public PetOwner toPetOwner(PetOwnerEntity petOwnerEntity) {
        return mapper.map(petOwnerEntity, PetOwner.class);
    }
}
