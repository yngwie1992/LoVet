package com.lovet.petowners.gateways;

import com.lovet.Role;
import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.gateways.entities.PetOwnerEntity;
import com.lovet.petowners.gateways.entities.mappers.PetOwnerGatewayMapper;
import com.lovet.petowners.gateways.repositories.PetOwnersRepository;
import com.lovet.petowners.usecases.owners.gateways.PetOwnerCommandGateway;
import com.lovet.petowners.usecases.owners.gateways.PetOwnersQueryGateway;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DatabasePetOwnersGateway implements PetOwnerCommandGateway, PetOwnersQueryGateway {

    private final PetOwnersRepository petOwnersRepository;
    private final PetOwnerGatewayMapper petOwnerGatewayMapper;

    public DatabasePetOwnersGateway(PetOwnersRepository petOwnersRepository, PetOwnerGatewayMapper petOwnerGatewayMapper) {
        this.petOwnerGatewayMapper = petOwnerGatewayMapper;
        this.petOwnersRepository = petOwnersRepository;
    }

    @Override
    public void save(PetOwner owner) {
        PetOwnerEntity petOwnerEntity = petOwnerGatewayMapper.toPetOwnerEntity(owner);
        petOwnerEntity.setRole(Role.PET_OWNER);
        petOwnersRepository.save(petOwnerEntity);
    }

    @Override
    public void deleteByUsername(String username) {
        petOwnersRepository.deleteByCredentialsEmail(username);
    }

    @Override
    public List<PetOwner> loadAll() {
        List<PetOwnerEntity> petOwners = petOwnersRepository.findAll();
        return petOwners.stream().map(petOwnerGatewayMapper::toPetOwner).collect(Collectors.toList());
    }

    @Override
    public PetOwner loadByEmail(String email) {
        PetOwnerEntity petOwnerEntity = petOwnersRepository.findByCredentialsEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Pet owner with email [%s] not found",
                        email)));
        return petOwnerGatewayMapper.toPetOwner(petOwnerEntity);
    }

    @Override
    public boolean existsByEmail(String email) {
        return petOwnersRepository.existsByCredentialsEmail(email);
    }
}
