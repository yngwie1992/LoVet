package com.lovet.springapp.config.startup;

import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.domain.Speciality;
import com.lovet.doctors.usecases.SaveDoctors;
import com.lovet.petowners.domain.owners.PetOwner;
import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.domain.pets.PetType;
import com.lovet.petowners.usecases.owners.SavePetOwners;
import com.lovet.petowners.usecases.pets.SavePets;
import com.lovet.visits.domain.Visit;
import com.lovet.visits.usecases.SaveVisits;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Set;

@Component
public class StartupApplicationListener implements
        ApplicationListener<ContextRefreshedEvent> {

    private final PasswordEncoder passwordEncoder;
    private final SaveDoctors saveDoctors;
    private final SavePetOwners savePetOwners;
    private final SavePets savePets;
    private final SaveVisits saveVisits;

    public StartupApplicationListener(PasswordEncoder passwordEncoder, SaveDoctors saveDoctors, SavePetOwners savePetOwners, SavePets savePets, SaveVisits saveVisits) {
        this.passwordEncoder = passwordEncoder;
        this.saveDoctors = saveDoctors;
        this.savePetOwners = savePetOwners;
        this.savePets = savePets;
        this.saveVisits = saveVisits;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        //DOCTORS
        Doctor nowakowska = Doctor.create()
                .withFirstName("Adrianna")
                .withLastName("Nowakowska")
                .withCredentials("nowakowska@lovet.com", passwordEncoder.encode("2403018"))
                .andSpecialities(Set.of(Speciality.DOGS));

        Doctor ciaputa = Doctor.create()
                .withFirstName("Adam")
                .withLastName("Ciaputa")
                .withCredentials("ciaputa@lovet.com", passwordEncoder.encode("2403018"))
                .andSpecialities(Set.of(Speciality.CATS));

        Doctor dziwak  = Doctor.create()
                .withFirstName("Grzegorz")
                .withLastName("Dziwak")
                .withCredentials("dziwak@lovet.com", passwordEncoder.encode("2403018"))
                .andSpecialities(Set.of(Speciality.DOGS));

        saveDoctors.save(nowakowska);
        saveDoctors.save(ciaputa);
        saveDoctors.save(dziwak);

        //PET OWNERS
        PetOwner kosinski = PetOwner.create()
                .withCredentials("piotrkosinski1992@o2.pl", passwordEncoder.encode("2403018"))
                .withFirstName("Piotr")
                .withLastName("Kosinski")
                .andPhoneNumber(507732218);

        PetOwner kruszy = PetOwner.create()
                .withCredentials("2piotrkosinski1992@o2.pl", passwordEncoder.encode("2403018"))
                .withFirstName("Marzena")
                .withLastName("Kruszy")
                .andPhoneNumber(666555444);

        PetOwner duchynska = PetOwner.create()
                .withCredentials("3piotrkosinski1992@o2.pl", passwordEncoder.encode("2403018"))
                .withFirstName("Magda")
                .withLastName("Duchynska")
                .andPhoneNumber(777888999);

        PetOwner jackel = PetOwner.create()
                .withCredentials("4piotrkosinski1992@o2.pl", passwordEncoder.encode("2403018"))
                .withFirstName("Agnieszka")
                .withLastName("Jackel")
                .andPhoneNumber(999888777);

        savePetOwners.save(kosinski);
        savePetOwners.save(kruszy);
        savePetOwners.save(duchynska);
        savePetOwners.save(jackel);

        Pet pimpus = new Pet("Pimpus", PetType.DOG, LocalDate.of(2018, 12, 16), "piotrkosinski1992@o2.pl");
        Pet karotka = new Pet("Karotka", PetType.RABBIT, LocalDate.of(2019, 5, 11), "piotrkosinski1992@o2.pl");
        Pet saba = new Pet("Saba", PetType.DOG, LocalDate.of(2018, 1, 22), "2piotrkosinski1992@o2.pl");
        Pet pedro = new Pet("Pedro", PetType.CAT, LocalDate.of(2018, 12, 3), "2piotrkosinski1992@o2.pl");
        Pet kluszek = new Pet("Kluszek", PetType.RABBIT, LocalDate.of(2017, 4, 5), "piotrkosinski1992@o2.pl");
        Pet tosia = new Pet("Tosia", PetType.DOG, LocalDate.of(2016, 4, 4), "piotrkosinski1992@o2.pl");
        Pet gandalf = new Pet("Gandalf", PetType.CAT, LocalDate.of(2013, 1, 3), "4piotrkosinski1992@o2.pl");
        Pet cassandra = new Pet("Cassandra", PetType.CAT, LocalDate.of(2013, 2, 11), "4piotrkosinski1992@o2.pl");
        Pet berni = new Pet("Berni", PetType.DOG, LocalDate.of(2011, 8, 11), "3piotrkosinski1992@o2.pl");

        savePets.saveAll(Arrays.asList(pimpus, karotka, saba, pedro, kluszek, tosia, gandalf, cassandra, berni));

        //VISITS

/*        Visit visitPimpus1 = Visit.create()
                .withDate(LocalDateTime.of(2019, 6, 30, 12, 0))
                .withDescription("czyszczenie uszu")
                .withPet()
                .andDoctor();*/
    }
}
