drop table if exists user;
drop table if exists visit_entity;
drop table if exists pets;
drop table if exists pet_owners;
drop table if exists doctor_specialities;
drop table if exists doctor_entity;

create table doctor_entity
(
    id         BINARY(16) not null,
    primary key (id)
);

create table pet_owners
(
    phone_number bigint,
    id           BINARY(16) not null,
    primary key (id)
);

create table pets
(
    id           bigint not null auto_increment,
    birth_date   date,
    name         varchar(255),
    pet_type     varchar(255),
    pet_owner_id BINARY(16),
    primary key (id)
);

alter table pets
    add constraint pets_pet_owner
    foreign key pets(pet_owner_id)
    references pet_owners(id);

create table user
(
    id       BINARY(16) not null,
    email    varchar(255),
    password varchar(255),
    role     varchar(255),
    first_name varchar(255),
    last_name  varchar(255),
    primary key (id)
);

create table visit_entity
(
    id                bigint not null auto_increment,
    visit_date        datetime,
    visit_description varchar(255),
    visit_status      varchar(255),
    doctor_id         BINARY(16),
    pet_id            bigint(16),
    primary key (id)
);

alter table visit_entity
    add constraint v_entity_doctor
    foreign key pet(doctor_id)
    references doctor_entity(id);

alter table visit_entity
    add constraint v_entity_pet
    foreign key visit_entity(pet_id)
    references pets(id);

create table doctor_specialities
(
  doctor_id   BINARY(16) not null,
  speciality  varchar(255)
);

