package com.lovet.visits.gateways.entities;

import com.lovet.doctors.gateways.entities.DoctorEntity;
import com.lovet.petowners.gateways.entities.PetEntity;
import com.lovet.visits.domain.VisitStatus;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class VisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "VISIT_DATE")
    private LocalDateTime date;

    private String visitDescription;

    @ManyToOne
    @JoinColumn(name = "PET_ID")
    @Transient
    private PetEntity pet;

    @ManyToOne
    @JoinColumn(name = "DOCTOR_ID")
    private DoctorEntity doctor;

    @Enumerated(EnumType.STRING)
    private VisitStatus visitStatus;

    private VisitEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getVisitDescription() {
        return visitDescription;
    }

    public void setVisitDescription(String visitDescription) {
        this.visitDescription = visitDescription;
    }

    public PetEntity getPet() {
        return pet;
    }

    public void setPet(PetEntity pet) {
        this.pet = pet;
    }

    public DoctorEntity getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorEntity doctor) {
        this.doctor = doctor;
    }

    public VisitStatus getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(VisitStatus visitStatus) {
        this.visitStatus = visitStatus;
    }
}
