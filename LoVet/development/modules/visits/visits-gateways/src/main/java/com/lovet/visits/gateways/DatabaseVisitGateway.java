package com.lovet.visits.gateways;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.gateways.entities.VisitEntity;
import com.lovet.visits.gateways.entities.mappers.VisitGatewayMapper;
import com.lovet.visits.gateways.repositories.VisitRepository;
import com.lovet.visits.usecases.gateways.VisitCommandGateway;
import com.lovet.visits.usecases.gateways.VisitQueryGateway;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class DatabaseVisitGateway implements VisitQueryGateway, VisitCommandGateway {

    private final VisitRepository visitRepository;
    private final VisitGatewayMapper mapper;

    public DatabaseVisitGateway(VisitRepository visitRepository, VisitGatewayMapper mapper) {
        this.visitRepository = visitRepository;
        this.mapper = mapper;
    }

    @Override
    public List<Visit> loadByPetId(Long petId) {
        return visitRepository.findByPetIdOrderByDateDesc(petId);
    }

    @Override
    public Visit loadById(Long id) {
        VisitEntity visitEntity = visitRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Visit id [%s] not found",
                        id)));
        return mapper.toVisit(visitEntity);
    }

    @Override
    public List<Visit> loadAllByDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd) {
        return visitRepository.findAllBetweenDayRange(dayBegin, dayEnd);
    }

    @Override
    public List<Visit> loadAllFromDoctorBetweenDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd, UUID doctorId) {
        return visitRepository.findAllFromDoctorBetweenDayRange(dayBegin, dayEnd, doctorId);
    }

    @Override
    public List<Visit> loadAllByDate(LocalDateTime date) {
        return visitRepository.findAllByDate(date);
    }

    @Override
    public void save(Visit visit) {
        visitRepository.save(mapper.toVisitEntity(visit));
    }




}
