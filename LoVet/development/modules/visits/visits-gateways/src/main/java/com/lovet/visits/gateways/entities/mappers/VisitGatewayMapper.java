package com.lovet.visits.gateways.entities.mappers;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.gateways.entities.VisitEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class VisitGatewayMapper {

    private ModelMapper mapper = new ModelMapper();

    public VisitEntity toVisitEntity(Visit visit) {
        return mapper.map(visit, VisitEntity.class);
    }

    public Visit toVisit(VisitEntity visitEntity) {
        return mapper.map(visitEntity, Visit.class);
    }
}
