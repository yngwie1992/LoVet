package com.lovet.visits.gateways.repositories;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.gateways.entities.VisitEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface VisitRepository extends JpaRepository<VisitEntity, Long> {
    //TODO DUMMY IMPL
    @Query("SELECT visit from VisitEntity visit")
    List<Visit> findByPetIdOrderByDateDesc(Long petId);

    @Query("SELECT visit FROM VisitEntity visit WHERE visit.date BETWEEN :dayBegin and :dayEnd ORDER BY visit.date ASC")
    List<Visit> findAllBetweenDayRange(
            @Param("dayBegin") LocalDateTime dayBegin,
            @Param("dayEnd") LocalDateTime dayEnd);

    @Query("SELECT visit FROM VisitEntity visit WHERE visit.date BETWEEN :dayBegin and :dayEnd AND visit.doctor.id.value = " +
            ":doctorId " +
            "ORDER BY " +
            "visit.date ASC")
    List<Visit> findAllFromDoctorBetweenDayRange(
            @Param("dayBegin") LocalDateTime dayBegin,
            @Param("dayEnd") LocalDateTime dayEnd,
            @Param("doctorId") UUID doctorId);

    @Query("SELECT visit FROM VisitEntity visit WHERE visit.date = :date")
    List<Visit> findAllByDate(@Param("date") LocalDateTime date);


    @Query(value = "SELECT visit.date FROM VisitEntity visit WHERE visit.date BETWEEN :dayBegin and :dayEnd ")
    List<LocalDateTime> findBookedVisitDatesByDayRange(@Param("dayBegin") LocalDateTime dayBegin,
                                                       @Param("dayEnd") LocalDateTime dayEnd);
}
