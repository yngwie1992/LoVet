package com.lovet.visits.web.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.lovet.EntityId;
import com.lovet.doctors.usecases.LoadDoctors;
import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.LoadPets;
import com.lovet.visits.domain.Visit;

@Component
public class VisitCommandDTOMapper {

    private final ModelMapper mapper = new ModelMapper();
    private final LoadDoctors loadDoctors;
    private final LoadPets loadPets;

    public VisitCommandDTOMapper(LoadDoctors loadDoctors, LoadPets loadPets) {
        this.loadDoctors = loadDoctors;
        this.loadPets = loadPets;
        mapper.addConverter(convertPetIdToPet());
        mapper.addConverter(stringDateToLocalDateTime());
    }

    public Visit toEntity(VisitCommandDTO visitCommandDTO) {
        Visit visit = mapper.map(visitCommandDTO, Visit.class);
        visit.setDoctor(loadDoctors.loadById(EntityId.fromString(visitCommandDTO.getDoctorId())));
        return visit;
    }

    private AbstractConverter<Long, Pet> convertPetIdToPet() {
        return new AbstractConverter<Long, Pet>() {
            protected Pet convert(Long petId) {
                return loadPets.loadById(petId);
            }
        };
    }

    private AbstractConverter<String, LocalDateTime> stringDateToLocalDateTime() {
        return new AbstractConverter<String, LocalDateTime>() {
            protected LocalDateTime convert(String localDateTime) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                return LocalDateTime.parse(localDateTime, formatter);
            }
        };
    }
}
