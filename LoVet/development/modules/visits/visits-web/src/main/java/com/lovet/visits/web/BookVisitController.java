package com.lovet.visits.web;

import com.lovet.visits.services.SaveVisitsProxy;
import com.lovet.visits.usecases.SaveVisits;
import com.lovet.visits.web.dto.VisitCommandDTO;
import com.lovet.visits.web.dto.VisitCommandDTOMapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Visits")
public class BookVisitController {

    private final SaveVisits saveVisit;
    private final VisitCommandDTOMapper visitCommandDTOMapper;

    public BookVisitController(SaveVisitsProxy saveVisit, VisitCommandDTOMapper visitCommandDTOMapper) {
        this.saveVisit = saveVisit;
        this.visitCommandDTOMapper = visitCommandDTOMapper;
    }

    @PostMapping("/visits/book")
    public void saveVisit(@RequestBody VisitCommandDTO visitCommandDTO) {
        saveVisit.book(visitCommandDTOMapper.toEntity(visitCommandDTO));
    }
}
