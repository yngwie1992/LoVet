package com.lovet.visits.web.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class VisitQueryDTO {

    private String visitDescription;

    private Long petId;

    @ApiModelProperty(
            example = "Puciek"
    )
    private String petName;

    @ApiModelProperty(
            example = "2018-07-26 10:30"
    )
    private String date;

    @ApiModelProperty(
            example = "c035069a-872c-4c85-a1fc-d0aa59c4418c"

    )
    private String doctorId;

    @NotNull
    @NotEmpty
    @ApiModelProperty(
            example = "Nowakowska"

    )
    private String doctorLastName;

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getVisitDescription() {
        return visitDescription;
    }

    public void setVisitDescription(String visitDescription) {
        this.visitDescription = visitDescription;
    }

    public Long getPetId() {
        return petId;
    }

    public void setPetId(Long petId) {
        this.petId = petId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
