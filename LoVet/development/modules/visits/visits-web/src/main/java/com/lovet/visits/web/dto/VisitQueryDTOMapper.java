package com.lovet.visits.web.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import com.lovet.EntityId;
import com.lovet.doctors.domain.Doctor;
import com.lovet.doctors.usecases.LoadDoctors;
import com.lovet.petowners.domain.pets.Pet;
import com.lovet.petowners.usecases.pets.LoadPets;
import com.lovet.visits.domain.Visit;

@Component
public class VisitQueryDTOMapper {

    private final ModelMapper mapper = new ModelMapper();
    private final LoadPets loadPets;
    private final LoadDoctors loadDoctors;

    public VisitQueryDTOMapper(LoadPets loadPets, LoadDoctors loadDoctors) {
        this.loadPets = loadPets;
        this.loadDoctors = loadDoctors;
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        mapper.addConverter(convertPetIdToPet());
        mapper.addConverter(convertDoctorIdToDoctor());
        mapper.addConverter(convertIdToDoctor());
        mapper.addConverter(stringDateToLocalDateTime());
    }

    public Visit toEntity(VisitQueryDTO visitQueryDTO) {
        return mapper.map(visitQueryDTO, Visit.class);
    }

    private AbstractConverter<Long, Pet> convertPetIdToPet() {
        return new AbstractConverter<Long, Pet>() {
            protected Pet convert(Long petId) {
                return loadPets.loadById(petId);
            }
        };
    }

    public VisitQueryDTO toDTO(Visit visit) {
        return mapper.map(visit, VisitQueryDTO.class);
    }

    private AbstractConverter<String, Doctor> convertDoctorIdToDoctor() {
        return new AbstractConverter<String, Doctor>() {
            protected Doctor convert(String id) {
                return loadDoctors.loadById(EntityId.fromString(id));
            }
        };
    }

    private AbstractConverter<Doctor, String> convertIdToDoctor() {
        return new AbstractConverter<Doctor, String>() {
            protected String convert(Doctor doctor) {
                return doctor.getId().getValue().toString();
            }
        };
    }

    private AbstractConverter<String, LocalDateTime> stringDateToLocalDateTime() {
        return new AbstractConverter<String, LocalDateTime>() {
            protected LocalDateTime convert(String localDateTime) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                return LocalDateTime.parse(localDateTime, formatter);
            }
        };
    }
}
