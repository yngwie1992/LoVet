package com.lovet.visits.web;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.usecases.LoadVisit;
import com.lovet.visits.web.dto.VisitQueryDTO;
import com.lovet.visits.web.dto.VisitQueryDTOMapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Visits")
public class LoadVisitController {

    private final LoadVisit loadVisit;
    private final VisitQueryDTOMapper mapper;

    public LoadVisitController(LoadVisit loadVisit, VisitQueryDTOMapper mapper) {
        this.loadVisit = loadVisit;
        this.mapper = mapper;
    }

    @GetMapping("visits/{id}")
    public List<VisitQueryDTO> getAllByPetId(@PathVariable Long id) {
        List<Visit> visits = loadVisit.loadAllByPetId(id);
        return visits.stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("visits/visit/{id}")
    public VisitQueryDTO getByVisitId(@PathVariable Long id) {
        Visit visit = loadVisit.loadById(id);
        return mapper.toDTO(visit);
    }

    @GetMapping("visits/visit/date/{visitDate}")
    public List<VisitQueryDTO> getAllByDay(@PathVariable String visitDate) {

        LocalDateTime dayMin = parseDateTimeFromString(visitDate);
        LocalDateTime dayMax = dayMin.with(LocalTime.MAX);
        List<Visit> visits = loadVisit.loadAllByDayRange(dayMin, dayMax);

        return visits.stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("visits/visit/date/{visitDate}/{doctorId}")
    public List<VisitQueryDTO> getAllByDayFromDoctor(@PathVariable String visitDate,
                                                     @PathVariable String doctorId) {

        LocalDateTime dayMin = parseDateTimeFromString(visitDate);
        LocalDateTime dayMax = dayMin.with(LocalTime.MAX);
        List<Visit> visits = loadVisit.loadAllFromDoctorBetweenDayRange(dayMin, dayMax, UUID.fromString(doctorId));
        return visits.stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
    }

    private LocalDateTime parseDateTimeFromString(String localDateTimeString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
        LocalDate localDate = LocalDate.parse(localDateTimeString, formatter);
        return localDate.atStartOfDay();
    }
}

