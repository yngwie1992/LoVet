package com.lovet.visits.web;

import com.lovet.visits.services.SaveVisitsProxy;
import com.lovet.visits.web.dto.VisitQueryDTO;
import com.lovet.visits.web.dto.VisitQueryDTOMapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Visits")
public class SaveVisitController {

    private final SaveVisitsProxy saveVisit;
    private final VisitQueryDTOMapper visitQueryDTOMapper;

    public SaveVisitController(SaveVisitsProxy saveVisit, VisitQueryDTOMapper visitQueryDTOMapper) {
        this.saveVisit = saveVisit;
        this.visitQueryDTOMapper = visitQueryDTOMapper;
    }

    @PostMapping("/visits")
    public void saveVisit(@RequestBody VisitQueryDTO visitQueryDTO) {
        saveVisit.save(visitQueryDTOMapper.toEntity(visitQueryDTO));
    }
}
