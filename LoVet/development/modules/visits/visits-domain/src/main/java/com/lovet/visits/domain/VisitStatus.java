package com.lovet.visits.domain;

public enum VisitStatus {
    RESERVED, ACCOMPLISHED
}
