package com.lovet.visits.domain;

import com.lovet.doctors.domain.Doctor;
import com.lovet.petowners.domain.pets.Pet;

import java.time.LocalDateTime;

public class VisitBuilder implements
        VisitBuilderInterfaces.WithDateInterface,
        VisitBuilderInterfaces.WithDescriptionInterface,
        VisitBuilderInterfaces.WithPetInterface,
        VisitBuilderInterfaces.WithDoctorInterface {

    private Visit visit;

    VisitBuilder() {
        visit = new Visit();
    }

    @Override
    public VisitBuilderInterfaces.WithDescriptionInterface withDate(LocalDateTime date) {
        visit.setDate(date);
        return this;
    }

    @Override
    public VisitBuilderInterfaces.WithPetInterface withDescription(String description) {
        visit.setVisitDescription(description);
        return this;
    }

    @Override
    public VisitBuilderInterfaces.WithDoctorInterface withPet(Pet pet) {
        visit.setPet(pet);
        return this;
    }

    @Override
    public Visit andDoctor(Doctor doctor) {
        visit.setDoctor(doctor);
        return visit;
    }
}
