package com.lovet.visits.domain;

import com.lovet.doctors.domain.Doctor;
import com.lovet.petowners.domain.pets.Pet;

import javax.persistence.*;
import java.time.LocalDateTime;

public class Visit {

    private Long id;
    private LocalDateTime date;
    private String visitDescription;
    private Pet pet;
    private Doctor doctor;
    private VisitStatus visitStatus;

    Visit() {
    }

    public static VisitBuilderInterfaces.WithDateInterface create() {
        return new VisitBuilder();
    }

    public Long getId() {
        return id;
    }

    public String getVisitDescription() {
        return visitDescription;
    }

    void setVisitDescription(String visitDescription) {
        this.visitDescription = visitDescription;
    }

    public Pet getPet() {
        return pet;
    }

    void setPet(Pet pet) {
        this.pet = pet;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public LocalDateTime getDate() {
        return date;
    }

    void setDate(LocalDateTime date) {
        this.date = date;
    }

    public VisitStatus getVisitStatus() {
        return visitStatus;
    }

    void setVisitStatus(VisitStatus visitStatus) {
        this.visitStatus = visitStatus;
    }
}
