package com.lovet.visits.domain;

import com.lovet.doctors.domain.Doctor;
import com.lovet.petowners.domain.pets.Pet;

import java.time.LocalDateTime;

final class VisitBuilderInterfaces {

    public interface WithDateInterface {
        WithDescriptionInterface withDate(LocalDateTime date);
    }

    public interface WithDescriptionInterface {
        WithPetInterface withDescription(String description);
    }

    public interface WithPetInterface {
        WithDoctorInterface withPet(Pet pet);
    }

    public interface WithDoctorInterface {
        Visit andDoctor(Doctor doctor);
    }
}
