package com.lovet.visits.services;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.usecases.SaveVisits;
import com.lovet.visits.usecases.gateways.VisitCommandGateway;
import com.lovet.visits.usecases.impl.SaveVisitsUsecase;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SaveVisitsProxy implements SaveVisits {

    private final SaveVisits saveVisits;

    public SaveVisitsProxy(VisitCommandGateway visitCommandGateway) {
        this.saveVisits = new SaveVisitsUsecase(visitCommandGateway);
    }

    @Transactional
    @Override
    public void save(Visit visit) {
        saveVisits.save(visit);
    }

    @Transactional
    @Override
    public void book(Visit visit) {
        saveVisits.book(visit);
    }
}
