package com.lovet.visits.services;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.usecases.LoadVisit;
import com.lovet.visits.usecases.gateways.VisitQueryGateway;
import com.lovet.visits.usecases.impl.LoadVisitUsecase;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class LoadVisitProxy implements LoadVisit {

    private final LoadVisit loadVisit;

    public LoadVisitProxy(VisitQueryGateway visitQueryGateway) {
        this.loadVisit = new LoadVisitUsecase(visitQueryGateway);
    }

    @Transactional
    @Override
    public List<Visit> loadAllByPetId(Long id) {
        return loadVisit.loadAllByPetId(id);
    }

    @Override
    public Visit loadById(Long id) {
        return loadVisit.loadById(id);
    }

    @Override
    public List<Visit> loadAllByDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd) {
        return loadVisit.loadAllByDayRange(dayBegin, dayEnd);
    }

    @Override
    public List<Visit> loadAllFromDoctorBetweenDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd, UUID doctorId) {
        return loadVisit.loadAllFromDoctorBetweenDayRange(dayBegin, dayEnd, doctorId);
    }

    @Override
    public List<Visit> loadAllByDate(LocalDateTime date) {
        return loadVisit.loadAllByDate(date);
    }
}
