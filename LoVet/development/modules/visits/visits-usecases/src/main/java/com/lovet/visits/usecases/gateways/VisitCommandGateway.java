package com.lovet.visits.usecases.gateways;

import com.lovet.visits.domain.Visit;

public interface VisitCommandGateway {

    void save(Visit visit);
}
