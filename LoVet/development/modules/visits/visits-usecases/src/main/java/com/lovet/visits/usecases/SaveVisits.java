package com.lovet.visits.usecases;

import com.lovet.visits.domain.Visit;

public interface SaveVisits {

    void save(Visit visit);

    void book(Visit visit);
}
