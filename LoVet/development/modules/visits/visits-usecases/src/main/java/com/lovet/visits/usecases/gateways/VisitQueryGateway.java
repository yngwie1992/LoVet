package com.lovet.visits.usecases.gateways;

import com.lovet.visits.domain.Visit;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface VisitQueryGateway {

    List<Visit> loadByPetId(Long petId);

    Visit loadById(Long id);

    List<Visit> loadAllByDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd);

    List<Visit> loadAllFromDoctorBetweenDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd, UUID doctorId);

    List<Visit> loadAllByDate(LocalDateTime date);
}
