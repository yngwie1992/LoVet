package com.lovet.visits.usecases.impl;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.usecases.SaveVisits;
import com.lovet.visits.usecases.gateways.VisitCommandGateway;

public class SaveVisitsUsecase implements SaveVisits {

    private final VisitCommandGateway visitCommandGateway;

    public SaveVisitsUsecase(VisitCommandGateway visitCommandGateway) {
        this.visitCommandGateway = visitCommandGateway;
    }

    @Override
    public void save(Visit visit) {
        visit.setAccomplished(true);
        visitCommandGateway.save(visit);
    }

    @Override
    public void book(Visit visit) {
        visit.setBooked(true);
        visitCommandGateway.save(visit);
    }
}
