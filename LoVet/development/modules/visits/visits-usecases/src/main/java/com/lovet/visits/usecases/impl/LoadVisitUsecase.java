package com.lovet.visits.usecases.impl;

import com.lovet.visits.domain.Visit;
import com.lovet.visits.usecases.LoadVisit;
import com.lovet.visits.usecases.gateways.VisitQueryGateway;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class LoadVisitUsecase implements LoadVisit {

    private final VisitQueryGateway visitQueryGateway;

    public LoadVisitUsecase(VisitQueryGateway visitQueryGateway) {
        this.visitQueryGateway = visitQueryGateway;
    }

    @Override
    public List<Visit> loadAllByPetId(Long id) {
        return visitQueryGateway.loadByPetId(id);
    }

    @Override
    public Visit loadById(Long id) {
        return visitQueryGateway.loadById(id);
    }

    @Override
    public List<Visit> loadAllByDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd) {
        return visitQueryGateway.loadAllByDayRange(dayBegin, dayEnd);
    }

    @Override
    public List<Visit> loadAllFromDoctorBetweenDayRange(LocalDateTime dayBegin, LocalDateTime dayEnd, UUID doctorId) {
        return visitQueryGateway.loadAllFromDoctorBetweenDayRange(dayBegin, dayEnd, doctorId);
    }

    @Override
    public List<Visit> loadAllByDate(LocalDateTime date) {
        return visitQueryGateway.loadAllByDate(date);
    }
}
